<?php $__env->startSection('titulo', "Perfil"); ?>

<?php $__env->startSection('contenido'); ?>
<section>

    <div id="head">
        <div class="line">
            <h1>Tu perfil</h1>
        </div>
    </div>

    <?php if(session('message.level')): ?>
    <div class="alert alert-<?php echo e(session('message.level')); ?>">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo e(session('message.content')); ?>

    </div>
    <?php endif; ?>
    <div id="content" class="left-align contact-page container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div id="perfilResultados" class="col-md-12">

                    <p></p>
                </div>
                <div id="perfilGraficos" class="col-md-12">
                    <div id="exTab2" class="container-fluid">	
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a  href="#1" data-toggle="tab">Resultados en tablas</a>
                            </li>
                            <li><a href="#2" data-toggle="tab" id="graficoPie">Without clearfix</a>
                            </li>
                            <li><a href="#3" data-toggle="tab">Solution</a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="1">
                                <h2>Aquí podrás ver tus progresos mediante las siguientes gráficas que te presento:</h2>
                                <table>
                                    <tr>
                                        <th>Juego</th>
                                        <th>Puntos</th>
                                        <th>Record</th>
                                        <th>Veces jugado</th>
                                        <th>Última vez jugado</th>
                                    </tr>
                                    <?php $__currentLoopData = $jugadasIndividuales; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jugadaIndividual): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($jugadaIndividual->nombre); ?></td>
                                        <td><?php echo e($jugadaIndividual->puntuacion); ?></td>
                                        <td><?php echo e($jugadaIndividual->puntMax); ?></td>
                                        <td><?php echo e($jugadaIndividual->vecesJugado); ?></td>
                                        <td><?php echo e($jugadaIndividual->updated_at); ?></td>
                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </table>
                                <?php if(session()->get('usuarioRol') === "Profesor"): ?>
                                <h2>Y como profesor tienes acceso a las estadísticas de tus alumnos. Recuerda darles tu código para que puedan agregarte como tutor. </h2>
                                <table>
                                    <tr>
                                        <th>Alumno</th>
                                        <th>Juego</th>
                                        <th>Puntos</th>
                                        <th>Record</th>
                                        <th>Última vez jugado</th>
                                    </tr>
                                    <?php $__currentLoopData = $juegos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $juego): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($juego->email); ?></th>
                                    <td><?php echo e($juego->nombre); ?></td>
                                    <td><?php echo e($juego->puntuacion); ?></td>
                                    <td><?php echo e($juego->puntMax); ?></td>
                                    <td><?php echo e($juego->updated_at); ?></td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </table>

                            <?php endif; ?>
                        </div>
                        <div class="tab-pane center-block" id="2">
                            <h3>Graficas</h3>
                            <div id="canvas-container" class="" style="width:35%">
                                <canvas id="resultadoBarras" width="300px" height="250px"></canvas>
                            </div>
                        </div>
                        <div class="tab-pane" id="3">
                            <h3>add clearfix to tab-content (see the css)</h3>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <form class = "form-inline pull-right" action="<?php echo e(route('user.update', $usuario->id)); ?>" method="post" >
            <?php if(session()->get('usuarioRol') === "Alumno"): ?>
            <div class="form-group">
                <label for="CodigoTutorForm">Código de tu tutor: </label>
                <input class="form-control" type="text" name="codigoTutor" value="<?php echo e($usuario->user_id); ?>" id ="CodigoTutorForm"/>
            </div>
            <div class="form-group">
                <button class="btn btn-default botonForm" type="submit">Actualizar</button>
            </div>
            <?php else: ?>
            <div class="form-group">
                <label>Tu código como tutor:</label>
                <input class="form-control" type="text" name="codigoTutor" value="<?php echo e($usuario->id); ?>" disabled/>
            </div>
            <?php endif; ?>

            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>"> <!--IMPORTANTE para hacer funcionar el método store tal y como está puesto aquí -->
        </form>


        <script>
            $(document).ready(function(){
                $("#graficoPie").on("click", function(e) {
                     console.log("entro en click");
                $.ajax({
                    type: 'get',
                    url: "estadisticas/alumno",
                    dataType: 'json',
                    success: function(data) {
                        console.log("olakase");
                    },
                    error: function(error) {
                        console.log("error");
                    }
                    });
                }); //final del evento del ajax


            }); // final del onload

        </script>


        </section>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layoutTemplate', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>