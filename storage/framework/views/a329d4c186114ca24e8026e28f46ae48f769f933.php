<?php $__env->startSection('titulo', "Login"); ?>

<?php $__env->startSection('contenido'); ?>

<div class="col-md-offset-2 col-md-8 col-md-offset-2">
    <form action="" method="post" >
        <h3>Comprobación de usuario<h3>
        <div class="form-group">
            <label>Email</label>
            <input class="form-control" type="text" name="email" placeholder="email@dominio.com" required/>
        </div>
        <div class="form-group">
            <label>Contraseña</label>
            <input class="form-control" type="password" name="pass" value="admin" required/>
        </div>
        <div class="form-group pull-right">
            <button class="btn btn-default" type="submit">Acceder</button> 
            <button class="btn btn-default" type="reset">Borrar</button> 
        </div>
            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>"> <!--IMPORTANTE para hacer funcionar el método store tal y como está puesto aquí -->
    </form>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>