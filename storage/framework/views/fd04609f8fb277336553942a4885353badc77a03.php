<?php $__env->startSection('titulo', "¡Fusiona elementos!"); ?>

<?php $__env->startSection('contenido'); ?>
<section>

    <div id="head">
        <div class="line">
            <h1>¡Juega a fusionar elementos!</h1>
        </div>
    </div>
    <div id="content">
        <div class="line">
            <div class="heading">
                <div class="scores-container">
                    <div class="score-container fuenteNegra" id="score-container">0</div>
                    <div class="best-container fuenteNegra">0</div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="game-container">
                        <div class="game-message">
                            <p></p>
                            <div class="lower">
                                <a class="retry-button">¡Vuelve a intentarlo!</a>
                            </div>
                        </div>

                        <div class="grid-container" id="grid-container">
                            <div class="grid-row">
                                <div class="grid-cell"></div>
                                <div class="grid-cell"></div>
                                <div class="grid-cell"></div>
                                <div class="grid-cell"></div>
                            </div>
                            <div class="grid-row">
                                <div class="grid-cell"></div>
                                <div class="grid-cell"></div>
                                <div class="grid-cell"></div>
                                <div class="grid-cell"></div>
                            </div>
                            <div class="grid-row">
                                <div class="grid-cell"></div>
                                <div class="grid-cell"></div>
                                <div class="grid-cell"></div>
                                <div class="grid-cell"></div>
                            </div>
                            <div class="grid-row">
                                <div class="grid-cell"></div>
                                <div class="grid-cell"></div>
                                <div class="grid-cell"></div>
                                <div class="grid-cell"></div>
                            </div>
                        </div>

                        <div class="tile-container" id="tile-container">

                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <h1 class="title fuenteNegra">¡Na!</h1>
                    <p class="game-intro" id="game-intro">¿Nunca has querido probar lo que es la <strong>fusión nuclear</strong>?</p>
                    <div class="text-justify">
                        Con este juego quiero enseñarte de manera gráfica como cambian los elementos a lo largo de un mismo periodo (fila). Este juego simula una especie de fusión nuclear del estilo que ocurre dentro de nuestro Sol donde los átomos, superando su energía de repulsión gracias a la elevada temperatura y presión (recuerda que los núcleos son positivos).<br/> 
                        Las instrucciones son sencillas: junta dos átomos de hidrógeno para obtener uno de helio, dos de helio para conseguir otro de litio y así sucesivamente hasta alcanzar el máximo que permite el juego (en esta versión, el magnesio).
                    </div> 
                    <p class="pull-right">
                        Este código pertenece a : <a href="https://github.com/CyberZHG/">CyberZHG</a>
                    </p>

                </div>
            </div>

            <script src="<?php echo e(asset('js/juegoDos/animframe_polyfill.js')); ?>"></script>
            <script src="<?php echo e(asset('js/juegoDos/keyboard_input_manager.js')); ?>"></script>
            <script src="<?php echo e(asset('js/juegoDos/html_actuator.js')); ?>"></script>
            <script src="<?php echo e(asset('js/juegoDos/grid.js')); ?>"></script>
            <script src="<?php echo e(asset('js/juegoDos/tile.js')); ?>"></script>
            <script src="<?php echo e(asset('js/juegoDos/local_score_manager.js')); ?>"></script>
            <script src="<?php echo e(asset('js/juegoDos/game_manager.js')); ?>"></script>
            <script src="<?php echo e(asset('js/juegoDos/skin_chemistry.js')); ?>"></script>

            <script>
                var idUsuario = <?php echo e(session()->get('usuarioId')); ?>;
                var idJuego = 2
                function recogerResultados(puntuacion, top, usuario, juego) {
                    var ruta = $("#inicio").attr("href");
                    var params = {
                        'juego_id': juego,
                        'puntuacion': puntuacion,
                        'puntMax': top,
                        'user_id': usuario,
                    };
                    $.ajax({
                        type: 'post',
                        url: ruta+"/juegos/score",
                        dataType: 'json',
                        data: params,
                        success: function(data) {
                            console.log("llego aqui");
                        },
                        error: function(error) {
                            console.log(error);
                        }
                    });
                }
                //Esta función permite recuperar el contenido de un elemento sin el texto que contienen sus hijos
                $.fn.soloContenidoPadre = function() {
                    return $(this).clone()
                        .children()
                        .remove()
                        .end()
                        .text();
                };
            </script>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layoutTemplate', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>