<!DOCTYPE html>
<html lang="en-US">
    <head>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width" />
        <title><?php echo $__env->yieldContent('titulo','EduQuѥm en estado puro'); ?> | Química al alcance de todos</title>
        <link rel="shorcut icon" href="<?php echo e(asset('images/element.ico')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('template/css/components.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('template/css/responsee.css')); ?>">
        <!-- CUSTOM STYLE -->  
        <link rel="stylesheet" href="<?php echo e(asset('template/css/template-style.css')); ?>">
        <link href="<?php echo e(asset('css/old.css')); ?>" rel="stylesheet" type="text/css">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo e(asset('css/custom.css')); ?>">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js" type="text/javascript"></script>
        <!-- Include SmartWizard CSS -->
        <link href="<?php echo e(asset('css/smart_wizard.css')); ?>" rel="stylesheet" type="text/css" />

        <!-- Optional SmartWizard theme -->
        <link href="<?php echo e(asset('css/smart_wizard_theme_arrows.css')); ?>" rel="stylesheet" type="text/css" />


        <!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
    </head>
    <body class="size-1140">
        <!-- TOP NAV WITH LOGO -->  
        <header>
            <nav>
                <div class="line">
                    <div class="top-nav">              
                        <div class="logo hide-l">
                            <?php if(session()->get('usuarioMailCortado')): ?>
                            <a href="<?php echo e(route('perfil')); ?>"><strong><?php echo e(session()->get('usuarioMailCortado')); ?></strong></a>
                            <?php else: ?>
                            <a href="<?php echo e(route('login')); ?>">TU <br /><strong>PERFIL</strong></a>
                            <?php endif; ?>
                        </div>                  
                        <p class="nav-text">Menú</p>
                        <div class="top-nav s-12 l-5" >
                            <ul class="right top-ul chevron">
                                <li><a id="inicio" href="<?php echo e(route('indice')); ?>">Inicio</a>
                                </li>
                                <li><a href="<?php echo e(route('practica')); ?>">¡Practica!</a>
                                </li>
                            </ul>
                        </div>
                        <ul class="s-12 l-2">
                            <li class="logo hide-s hide-m">
                                <?php if(session()->get('usuarioMailCortado')): ?>
                                <a href="<?php echo e(route('perfil')); ?>"><strong><?php echo e(session()->get('usuarioMailCortado')); ?></strong></a>
                                <?php else: ?>
                                <a href="<?php echo e(route('login')); ?>">TU <br /><strong>PERFIL</strong></a>
                                <?php endif; ?>
                            </li>
                        </ul>
                        <div class="top-nav s-12 l-5">
                            <ul class="top-ul chevron">
                                <li>
                                    <a>Juegos</a>			    
                                    <ul>
                                        <li><a href="<?php echo e(route('juegoSimon')); ?>">¡Mendeleiev dice!</a>
                                        </li>
                                        <li><a href="<?php echo e(route('juego2048')); ?>">¡Fusiona elementos!</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="<?php echo e(route('documentacion')); ?>">Manual</a>
                                </li>
                                <?php if(session()->get('usuarioMail')): ?>
                                <li><a href="<?php echo e(route('salir')); ?>">Salir</a></li>
                                <?php endif; ?>
                                <?php if(!session()->get('usuarioMail')): ?>
                                <li><a href="<?php echo e(route('registrar')); ?>">Registrarse</a></li>
                                <?php endif; ?>
                            </ul> 
                        </div>
                    </div>
                </div>
            </nav>
        </header>

        <?php echo $__env->yieldContent('contenido'); ?>	

        <footer class="fixed-bottom">
            <div class="line">
                <div class="s-12 l-6">
                    <p>Copyright 2017 - Ringo S.L - Ivan Rodriguez Carrion
                    </p>
                </div>
                <div class="s-12 l-6">
                    <p class="right">
                    <hr/><pre>Proyecto final de curso - Licencia WTFPL <br /> Desarrollo de aplicaciones WEB - CEEDCV .</pre>
                    </p>
            </div>
            </div>
        </footer>
    </body>
</html>