<!DOCTYPE html>
<html lang="es">
    <head>
        <title><?php echo $__env->yieldContent('titulo','Química en estado puro'); ?> | Química al alcance de todos</title>
        <meta charset="UTF-8">
        <link rel="shorcut icon" href="<?php echo e(asset('images/element.ico')); ?>">
        <link href="<?php echo e(asset('css/old.css')); ?>" rel="stylesheet" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
        <style>
            body {background: url("<?php echo e(asset('images/fondo.png')); ?>"),repeat;}
        </style>

         <link rel="stylesheet" href="<?php echo e(asset('css/custom.css')); ?>">
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">
        <meta name="viewport" content="width=device-width, target-densitydpi=160dpi, initial-scale=1.0, maximum-scale=1, user-scalable=no, minimal-ui">
    </head>
    <body >
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Barra de navegación</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a id="inicio" href="<?php echo e(route('indice')); ?>">Inicio</a></li>
                        <li><a href="<?php echo e(route('practica')); ?>">Zona de testeo</a></li>
                        <li><a href="<?php echo e(route('juegoSimon')); ?>">Juegos</a></li>
                        <li><a href="">Sobre nosotros</a></li>
                        
                        <?php if(session()->get('usuarioMail')): ?>
                            <li><a href="<?php echo e(route('perfil')); ?>"><?php echo e(session()->get('usuarioMail')); ?></a></li>
                            <li><a href="<?php echo e(route('salir')); ?>">Salir</a></li>
                        <?php else: ?>                  
                            <li><a href="<?php echo e(route('login')); ?>" class="pull-right">Acceder</a></li>
                            <li><a href="<?php echo e(route('registrar')); ?>">Registrarse</a></li>
                        <?php endif; ?>
                        
                    </ul>
                </div>
            </div>
        </nav>

        <?php echo $__env->yieldContent('contenido'); ?>

        <footer class="col-md-12">
            <hr/><pre> Ringo S.L - Ivan Rodriguez Carrion - Proyecto final de curso - Desarrollo de aplicaciones WEB.</pre>
        </footer>
    </body>
</html>