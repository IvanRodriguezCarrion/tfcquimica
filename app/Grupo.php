<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grupo extends Model
{
    public $timestamps = false;
    protected $tabla = "Grupos";
    protected $fillable = ["id","nombre","descrip"];
    
    public function Elementos() {
        return $this->hasMany('App\Elemento');
    }
}
