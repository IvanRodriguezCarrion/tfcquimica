<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadisticaPractica extends Model
{

    protected $tabla = "Estadistica_Practicas";
    protected $fillable = ["id","estado","porcentaje", "user_id","practicas_id"];
    
    public function user() {
        return $this->belongsTo('App\User','user_id');
    }
    
    public function practica() {
        return $this->belongsTo('App\Practica','practicas_id');
    }
}
