<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Practica extends Model
{
    public $timestamps = false;
    protected $tabla = "Practicas";
    protected $fillable = [
       'id', 'descrip', 'grupo', 
    ];
    
    public function estadisticasPractica() {
        return $this->hasMany('App\EstadisticaPractica');
    }
}
