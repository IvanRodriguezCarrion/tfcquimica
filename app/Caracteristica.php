<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Caracteristica extends Model
{
    public $timestamps = false;
    protected $tabla = "Caracteristicas";
    protected $fillable = ["id","posicion","pAtomico","nGrupo","nPeriodo","descrip"];
    
    public function Elementos() {
        return $this->hasMany('App\Elemento');
    }
}
