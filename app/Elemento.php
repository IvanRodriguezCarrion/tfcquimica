<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Elemento extends Model
{
    public $timestamps = false;
    protected $tabla = "Elementos";
    protected $fillable = ["id","nombre","simbolo"];
    
    public function estadoOxidacion() {
        return $this->belongsTo('App\EstadoOxidacion',"estadosoxidacion_id");
    }
    
    public function grupo() {
        return $this->belongsTo('App\Grupo',"grupo_id");
    } 
    
    public function caracteristica() {
        return $this->belongsTo('App\Caracteristica',"caracteristica_id");
    }
    
}
