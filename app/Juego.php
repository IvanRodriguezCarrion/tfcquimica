<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Juego extends Model
{
    public $timestamps = false;
    protected $tabla = "Juegos";
    protected $fillable = [
       'id', 'nombre', 'dificultad', 'llamadaJuego', 'descrip', 
    ];
    
    public function estadisticas() {
        return $this->hasMany('App\Estadistica');
    }
}

