<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoOxidacion extends Model
{
    public $timestamps = false;
    protected $tabla = "EstadosOxidacion";
    protected $fillable = ["id","valor","carga"];
    
    public function Elementos() {
        return $this->hasMany('App\Elemento');
    }
}
