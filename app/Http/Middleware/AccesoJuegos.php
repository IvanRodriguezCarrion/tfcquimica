<?php

namespace App\Http\Middleware;

use Closure;

class AccesoJuegos
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!session()->get('auth') || session()->get('auth')!=="autenticado") {
            $request->session()->flash('message.level', 'warning');
            $request->session()->flash('message.content', 'Para poder entrar debes logear primero');
            return redirect()->route('login');
        }
        return $next($request);
    }
}
