<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Juego;
use App\EstadisticaPractica;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Response;

class ControladorEstadisticaPractica extends Controller
{

    public function recuperarPosicionPractica() {
        $practicasPosicion =  DB::table('estadistica_practicas')
            ->join('practicas', 'practicas.id', '=', 'estadistica_practicas.practicas_id')
            ->select("estadistica_practicas.estado", "estadistica_practicas.posicion", "practicas.grupo as grupo")
            ->where('user_id', Auth::user()->id)->get();       
        //dd($practicasPosicion);
        return Response::json($practicasPosicion);

    } 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {   //dd($request->practicas_id);
        $id = EstadisticaPractica::
        where('practicas_id',  $request->practicas_id)
            ->where('user_id', $request->user_id)
            ->select("id")
            ->get();
        //dd($id);
        $resultado = EstadisticaPractica::find($id);
        
        if ($request->posicion === "#step-7") {
            $resultado->estado = 1;
        } else {
            $resultado->estado = 0;
        }
        //dd($request->posicion);
        $resultado->posicion     = $request->posicion;
        $resultado->practicas_id = $request->practicas_id;
        $resultado->practicas_id = $request->practicas_id;
        $resultado->user_id      = $request->user_id;
        $resultado->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
