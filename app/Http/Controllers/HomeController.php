<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use App\EstadisticaJuego;
use App\Juego;
use App\Practica;
use App\EstadisticaPractica;
use App\Http\Controllers\ControladorPrincipal; 

class HomeController extends Controller
{
    /**
     * Create a new controller instsance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        //$usuario = DB::table("users")->where("id", Auth::user()->id);
        $usuario = User::where('id', Auth::user()->id)->first();
        //Característcas de la sesión en curso dentro de las variables de sesión, útil para
        //tener controlados algunos campos y para indicar quien está logueado. 
        $userMail   = Auth::user()->email;
        $userId     = Auth::user()->id;
        $userRol    = Auth::user()->rol;
        session(['auth'          => "autenticado"]);
        session(['usuarioMail'          => $userMail]);
        session(['usuarioId'            => $userId]);
        session(['usuarioRol'           => $userRol]);
        $prueba = explode('@', $userMail);
        //dd($prueba[0]);
        session(['usuarioMailCortado'   => $prueba[0]]);
        
        $controladorPrincipal = new ControladorPrincipal();
        
        return $controladorPrincipal->perfil();
    }

}
