<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Laracasts\Flash\Flash;
use App\User;
use App\EstadisticaJuego;
use App\Juego;
use App\Practica;
use App\EstadisticaPractica;

class ControladorUser extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->password_confirmation === $request->password) {
            $users = DB::table('users')->where('email', $request->email)->first();

            if ($users === null) {
                if ($request->input('rol')!==null) {
                    $user = new user();
                    $user->nick = "nick";
                    $user->rol = $request->input('rol');
                    $user->email = $request->email;
                    $user->password = bcrypt($request->password);
                    $user->nombre = null;
                    $user->fechaNac = null;
                    //$user->timestamps();
                    $user->save();

                    $user = DB::table('users')->where('email', $request->email)->first();

                    $contador = 0;
                    foreach (Juego::where('id','>',0)->cursor() as $juego) {
                        $contador = DB::table("estadistica_juegos")->max("id");
                        $caracteristicaJuego = new EstadisticaJuego();
                        $caracteristicaJuego->id = $contador+1;
                        $caracteristicaJuego->estado = 0;
                        $caracteristicaJuego->puntuacion = 0;
                        $caracteristicaJuego->puntMax = 0;
                        $caracteristicaJuego->vecesJugado = 0;
                        $caracteristicaJuego->user_id = $user->id;
                        //$caracteristicaJuego->timestamps();
                        $caracteristicaJuego->juego_id = $juego->id;

                        $caracteristicaJuego->save();
                    }

                    foreach (Practica::where('id','>',0)->cursor() as $practica) {
                        $contador = DB::table("estadistica_practicas")->max("id");
                        $caracteristicaPractica = new EstadisticaPractica();
                        $caracteristicaPractica->id = $contador+1;
                        $caracteristicaPractica->estado = 0;
                        $caracteristicaPractica->posicion = "#step-1";
                        $caracteristicaPractica->user_id = $user->id;
                        $caracteristicaPractica->practicas_id = $practica->id;
                        //$caracteristicaPractica->timestamps();
                        $caracteristicaPractica->save();
                    }

                    $request->session()->flash('message.level', 'success');
                    $request->session()->flash('message.content', '¡Ya estás registrado!');
                    return redirect()->route('registrar');
                } else {
                    $request->session()->flash('message.level', 'danger');
                    $request->session()->flash('message.content', 'Debes seleccionar un rol');
                    return redirect()->route('registrar');
                }
            } else {
                $request->session()->flash('message.level', 'danger');
                $request->session()->flash('message.content', 'El mail ya está en uso');
                return redirect()->route('registrar');
            }
        } else {
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'Las contraseñas no coinciden');
            return redirect()->route('registrar');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user               = User::find($id);
        $user->nombre       = $request->nombre;
        $user->nick         = "nick";
        $user->FechaNac     = '1980-10-12';
        $user->user_id      = $request->codigoTutor;
        
        if (Auth::user()->rol == "Alumno") {
            $user->user_id = $request->codigoTutor;
        } else {
            $user->user_id = 1;
        }
        $user->save();

        $request->session()->flash('message.level', 'success');
        $request->session()->flash('message.content', '¡Cambios actualizados con éxito!!');
        
        return redirect()->route('login');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
