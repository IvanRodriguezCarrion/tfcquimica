<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;

class ControladorPrincipal extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('inicio');
    }

    public function practicas()
    {
        return view('practicas');
    }

    public function perfil()
    {
        $usuario = User::where('id', Auth::user()->id)->first();       
        $juegos = DB::table("users AS usuario")
                  ->join("estadistica_juegos AS EstJuego", "usuario.id", "=", "EstJuego.user_id")
                  ->join("juegos AS game", "game.id", "=", "EstJuego.juego_id")
                  ->select("usuario.id", "usuario.email", 
                           "game.nombre", 
                           "EstJuego.estado",
                           "EstJuego.puntuacion",
                           "EstJuego.vecesJugado",
                           "EstJuego.updated_at",
                           "EstJuego.puntMax")
                  ->where("usuario.user_id", Auth::user()->id)->get();
        
        $jugadasIndividuales = DB::table("users AS usuario")
                  ->join("estadistica_juegos AS EstJuego", "usuario.id", "=", "EstJuego.user_id")
                  ->join("juegos AS game", "game.id", "=", "EstJuego.juego_id")
                  ->select("usuario.id", "usuario.email", 
                           "game.nombre", 
                           "EstJuego.estado",
                           "EstJuego.puntuacion",
                           "EstJuego.vecesJugado",
                           "EstJuego.updated_at",
                           "EstJuego.puntMax")
                  ->where("usuario.id", Auth::user()->id)->get();
        
        return view('perfil')->with('usuario',$usuario)
                             ->with("juegos", $juegos)
                             ->with("jugadasIndividuales", $jugadasIndividuales);

    }

    public function salir() {
        Auth::logout();
        session()->flush();
        return view('inicio');
    }

    public function registrar()
    {
        return view('auth/register');
    }

    public function recuperarUsuario() {
        $usuarioActivo = Auth::id();
        return $usuarioActivo;
    }

    public function juegoSimon() {
        return view('juegoSimon');
    }
      public function juego2048() {
        return view('juego2048');
    }
    
    public function documentacion() {
        return response()->file('doc/docu.pdf');
    }

}
