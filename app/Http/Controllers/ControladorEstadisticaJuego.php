<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\EstadisticaJuego;
use App\User;
use App\Juego;

class ControladorEstadisticaJuego extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {   //dd($request->puntuacion);
        $id = EstadisticaJuego::
            where('juego_id'  ,   $request->juego_id)
            ->where('user_id' ,   $request->user_id)
            ->select("id")
            ->get();

        $resultado = EstadisticaJuego::find($id);
        $resultado->juego_id    = $request->juego_id;
        $resultado->estado      = 1;
        $resultado->user_id     = $request->user_id;
        $resultado->puntuacion  = $request->puntuacion;
        $resultado->vecesJugado  += 1;
        
        if ($request->puntMax) {
             $resultado->puntMax = $request->puntMax;
        } elseif ($resultado->puntMax < $request->puntuacion) {
            $resultado->puntMax = $request->puntuacion;
        };
        //dd($resultado);
        $resultado->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function recuperarGraficaPie()
   {   //dd("entro");
//        $vecesJugado = DB::table('Users')->select("Users.email", "Juegos.nombre", "Estadistica_juegos.vecesJugado") 
//            ->join('Estadistica_juegos', 'Users.id', '=', 'Estadistica_juegos.user_id')
//            ->join('Juegos', 'Juegos.id', '=', 'Estadistica_juegos.juego_id')
//            ->where("Users.id", "=", $request->id)
//            ->get();
//        dd($vecesJugado);
       //$elementos = Elemento::all();
       //return Response::json($vecesJugado);
    }
}
