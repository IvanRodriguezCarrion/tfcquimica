<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Elemento;
use App\Caracteristica;
use App\EstadoOxidacion;
use App\Grupo;
use Response;
use Illuminate\Support\Facades\DB;


class ControladorElemento extends Controller
{
   
    public function recuperarTodo()
    {
       $elementos = DB::table('elementos')->select("elementos.id AS id", "elementos.nombre AS nombreElemento", "elementos.simbolo AS simbolo","caracteristicas.nGrupo as grupo", "grupos.descrip as descripcionGrupo") ->join('estadosoxidacion', 'estadosoxidacion.id', '=', 'elementos.estadosOxidacion_id')
            ->join('caracteristicas', 'caracteristicas.id', '=', 'elementos.caracteristicas_id')
            ->join('grupos', 'grupos.id', '=', 'elementos.grupos_id')
            ->get();
        
       //$elementos = Elemento::all();
       return Response::json($elementos);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
}