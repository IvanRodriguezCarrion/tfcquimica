<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    protected $tabla = "Users";
    protected $fillable = [
       'id','nick', 'nombre', 'rol', 'email', 'fechaNac', 'password',
    ];
    protected $hidden = [
        'password',
    ];
    
    public function estadisticasJuego() {
        return $this->hasMany('App\EstadisticaJuego');
    }
    
    public function estadisticasPractica() {
        return $this->hasMany('App\EstadisticaPractica');
    }
}
