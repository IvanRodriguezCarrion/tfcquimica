<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadisticaJuego extends Model
{

    protected $tabla = "Estadisticas_Juegos";
    protected $fillable = ["id","estado","puntuacion", "puntMax", "vecesJugado", "user_id", "juego_id"];
    
    public function user() {
        return $this->belongsTo('App\User','user_id');
    }
    
    public function juego() {
        return $this->belongsTo('App\Juego','juego_id');
    }
}
