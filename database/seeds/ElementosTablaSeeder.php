<?php

use Illuminate\Database\Seeder;

class ElementosTablaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('Elementos')->insert([
            'nombre' => 'Hidrógeno',
            'simbolo' => 'H',
            'caracteristicas_id' => '1',
            'estadosoxidacion_id' => '1',
            'grupos_id' => '1',
        ]);

        DB::table('Elementos')->insert([
            'nombre' => 'Litio',
            'simbolo' => 'Li',
            'caracteristicas_id' => '2',
            'estadosoxidacion_id' => '1',
            'grupos_id' => '1',
        ]);

        DB::table('Elementos')->insert([
            'nombre' => 'Sodio',
            'simbolo' => 'Na',
            'caracteristicas_id' => '3',
            'estadosoxidacion_id' => '1',
            'grupos_id' => '1',
        ]);

        DB::table('Elementos')->insert([
            'nombre' => 'Litio',
            'simbolo' => 'Li',
            'caracteristicas_id' => '4',
            'estadosoxidacion_id' => '1',
            'grupos_id' => '1',
        ]);
    }
}
<?php

