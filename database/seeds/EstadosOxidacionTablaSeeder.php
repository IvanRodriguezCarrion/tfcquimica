<?php

use Illuminate\Database\Seeder;

class EstadosOxidacionTablaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('EstadosOxidacion')->insert([
            'id' => 1,
            'valor' => 1,
            'carga' => "Catión",
        ]);
        
        DB::table('EstadosOxidacion')->insert([
            'id' => 2,
            'valor' => 1,
            'carga' => "Cation",
        ]);
        
        DB::table('EstadosOxidacion')->insert([
            'id' => 3,
            'valor' => 3,
            'carga' => "Cation",
        ]);
        
        DB::table('EstadosOxidacion')->insert([
            'id' => 4,
            'valor' => 2,
            'carga' => "Cation",
        ]);
        
        DB::table('EstadosOxidacion')->insert([
            'id' => 4,
            'valor' => 4,
            'carga' => "Cation",
        ]);
        
        DB::table('EstadosOxidacion')->insert([
            'id' => 5,
            'valor' => 1,
            'carga' => "Cation",
        ]);
        
        DB::table('EstadosOxidacion')->insert([
            'id' => 5,
            'valor' => 3,
            'carga' => "Cation",
        ]);
        
        DB::table('EstadosOxidacion')->insert([
            'id' => 5,
            'valor' => 5,
            'carga' => "Cation",
        ]);
        
        DB::table('EstadosOxidacion')->insert([
            'id' => 6,
            'valor' => 1,
            'carga' => "Cation",
        ]);
        
        DB::table('EstadosOxidacion')->insert([
            'id' => 6,
            'valor' => 3,
            'carga' => "Cation",
        ]);
        
        DB::table('EstadosOxidacion')->insert([
            'id' => 6,
            'valor' => 5,
            'carga' => "Cation",
        ]);
        
        DB::table('EstadosOxidacion')->insert([
            'id' => 6,
            'valor' => 7,
            'carga' => "Cation",
        ]);
    }
}
