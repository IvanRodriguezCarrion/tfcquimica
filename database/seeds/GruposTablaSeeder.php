<?php

use Illuminate\Database\Seeder;

class GruposTablaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Grupos')->insert([
            'nGrupo' => 1,
            'nombre' => 'Alcalinos',
            'descrip' => "El grupo de los metales alcalinos está formados por los elementos que se sitúan en la primera columna (grupo) de la tabla periódica o sistema periódico, lo componen los elementos químicos litio (Li), sodio (Na), potasio (K), rubidio (Rb), cesio (Cs), francio (Fr). Pertenecen a la serie de elementos s, esto significa que los metales alcalinos tienen su electrón más externo en un orbital s, cada uno tiene solo un electrón en su nivel energético más externo (s), con buena tendencia a perderlo (esto es debido a que tienen poca afinidad electrónica, y baja energía de ionización), con lo que forman un ion monovalente, M+. Esta configuración electrónica compartida da como resultado que tengan propiedades características muy similares. De hecho, los metales alcalinos proporcionan el mejor ejemplo de tendencias de grupo en propiedades en la tabla periódica, con elementos que exhiben un comportamiento homólogo bien caracterizado.",
        ]);
        
        DB::table('Grupos')->insert([
            'nGrupo' => 2,
            'nombre' => 'Alcalinotérreos',
            'descrip' => "Los metales alcalinotérreos son un grupo de elementos que se encuentran situados en el grupo 2 de la tabla periódica y son los siguientes: berilio (Be), magnesio (Mg), calcio (Ca), estroncio (Sr), bario (Ba) y radio (Ra). Este último no siempre se considera, pues tiene un tiempo de vida media corta. El nombre «alcalinotérreos» proviene del nombre que recibían sus óxidos, «tierras», que tienen propiedades básicas (alcalinas). Poseen una electronegatividad ≤ 1,57 según la escala de Pauling. Características: son más duros que los metales alcalinos, tienen brillo y son buenos conductores eléctricos; menos reactivos que los alcalinos, buenos agentes reductores y forman compuestos iónicos. Todos ellos tienen dos (2) electrones en su capa más externa (electrones de Valencia).",
        ]);
    }
}
