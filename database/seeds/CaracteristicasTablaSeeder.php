<?php

use Illuminate\Database\Seeder;

class CaracteristicasTablaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Caracteristicas')->insert([
            'posicion' => 1,
            'pAtomico' => 1,
            'nGrupo' => 1,
            'nPeriodo' => 1,
            'img' => 'ele001',
            'descrip' => 'Es el elemento químico de número atómico 1, representado por el símbolo H. Con una masa atómica de 1,00794 (7) u, es el más ligero de la tabla de los elementos. Por lo general, se presenta en su forma molecular, formando el gas diatómico H2 en condiciones normales. Este gas es inflamable, incoloro, inodoro, no metálico e insoluble en agua. El hidrógeno es el elemento químico más abundante, al constituir aproximadamente el 75 % de la materia visible del universo.2 nota 1 En su secuencia principal, las estrellas están compuestas principalmente por hidrógeno en estado de plasma. El hidrógeno elemental es relativamente raro en la Tierra y es producido industrialmente a partir de hidrocarburos como, por ejemplo, el metano. La mayor parte del hidrógeno elemental se obtiene in situ, es decir, en el lugar y en el momento en que se necesita. Los mayores mercados del mundo disfrutan de la utilización del hidrógeno para el mejoramiento de combustibles fósiles (en el proceso de hidrocraqueo) y en la producción de amoníaco (principalmente para el mercado de fertilizantes). El hidrógeno puede obtenerse a partir del agua por un proceso de electrólisis, pero resulta un método mucho más caro que la obtención a partir del gas natural.'
        ]);

        
        DB::table('Caracteristicas')->insert([
            'posicion' => 2,
            'pAtomico' => 4,
            'nGrupo' => 18,
            'nPeriodo' => 1,
            'img' => 'ele002',
            'descrip' => 'El helio (en griego:ἥλιος helios,sol) es un elemento químico de número atómico 2, símbolo He y peso atómico estándar de 4,0026. Pertenece al grupo 18 de la tabla periódica de los elementos, ya que al tener el nivel de energía completo presenta las propiedades de un gas noble. Es decir, es inerte (no reacciona) y al igual que estos, es un gas monoatómico incoloro e inodoro que cuenta con el menor punto de ebullición de todos los elementos químicos y solo puede ser licuado bajo presiones muy grandes y no puede ser congelado. Durante un eclipse solar en 1868, el astrónomo francés Pierre Janssen observó una línea espectral amarilla en la luz solar que hasta ese momento era desconocida. Norman Lockyer observó el mismo eclipse y propuso que dicha línea era producida por un nuevo elemento, al cual llamó helio, con lo cual, tanto a Lockyer como a Janssen se les adjudicó el descubrimiento de este elemento. En 1903 se encontraron grandes reservas de helio en campos de gas natural en los Estados Unidos, país con la mayor producción de helio en el mundo.'
        ]);
        
        DB::table('Caracteristicas')->insert([
            'posicion' => 11,
            'pAtomico' => 23,
            'nGrupo' => 1,
            'nPeriodo' => 3,
            'img' => 'ele003',
            'descrip' => '...'
        ]);
        
        DB::table('Caracteristicas')->insert([
            'posicion' => 19,
            'pAtomico' => 39,
            'nGrupo' => 1,
            'nPeriodo' => 4,
            'img' => 'ele004',
            'descrip' => '...'
        ]);
        
    }
}
