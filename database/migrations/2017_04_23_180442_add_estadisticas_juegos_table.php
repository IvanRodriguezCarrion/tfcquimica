<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEstadisticasJuegosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Estadistica_Juegos', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('estado')->default(0);
            $table->integer('puntuacion')->default(0);
            $table->integer('puntMax')->default(0);
            $table->integer('vecesJugado')->default(0);
            $table->integer('user_id')->unsigned();
            $table->integer('juego_id')->unsigned();
            
            $table->foreign('user_id')->references('id')->on('Users')->onDelete('cascade');;
            $table->foreign('juego_id')->references('id')->on('Juegos');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Estadisticas');
    }
}
