<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEstadisticasPracticasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Estadistica_Practicas', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('estado');
            $table->string('posicion');
            
            $table->integer('user_id')->unsigned();
            $table->integer('practicas_id')->unsigned();
            
            $table->foreign('user_id')->references('id')->on('Users')->onDelete('cascade');;
            $table->foreign('practicas_id')->references('id')->on('Practicas');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Estadisticas');
    }
}
