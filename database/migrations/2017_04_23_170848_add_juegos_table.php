<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddJuegosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Juegos', function (Blueprint $table) {
            $table->increments('id');
            $table->String('nombre');
            $table->String('dificultad');
            $table->String('llamadaJuego');
            $table->String('descrip', 2000);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Juegos');
    }
}
