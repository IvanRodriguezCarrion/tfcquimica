<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCaracteristicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Caracteristicas', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('posicion');
            $table->integer('pAtomico');
            $table->integer('nGrupo');
            $table->integer('nPeriodo');
            $table->String('descrip', 2000);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Caracteristicas');
    }
}
