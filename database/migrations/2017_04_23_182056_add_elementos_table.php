<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddElementosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Elementos', function (Blueprint $table) {
            $table->increments('id');
            $table->String('nombre');
            $table->String('simbolo');
            $table->integer('caracteristicas_id')->unsigned();
            $table->integer('estadosoxidacion_id')->unsigned();
            $table->integer('grupos_id')->unsigned();
            
            $table->foreign('caracteristicas_id')->references('id')->on('Caracteristicas');
            $table->foreign('estadosoxidacion_id')->references('id')->on('EstadosOxidacion');
            $table->foreign('grupos_id')->references('id')->on('Grupos');   
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Elementos');
    }
}
