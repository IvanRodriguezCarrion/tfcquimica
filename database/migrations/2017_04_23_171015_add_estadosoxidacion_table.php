<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEstadosoxidacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('EstadosOxidacion', function (Blueprint $table) {
            $table->integer('id')->unsigned();
            $table->Integer('valor')->unsigned();
            $table->enum('carga', ['Anión', 'Catión'])->default('Anión');
            $table->primary(['id', 'valor']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('EstadosOxidacion');
    }
}
