-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-06-2017 a las 23:24:19
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyecto`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caracteristicas`
--

CREATE TABLE `caracteristicas` (
  `id` int(10) UNSIGNED NOT NULL,
  `posicion` int(11) NOT NULL,
  `pAtomico` int(11) NOT NULL,
  `nGrupo` int(11) NOT NULL,
  `nPeriodo` int(11) NOT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descrip` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `caracteristicas`
--

INSERT INTO `caracteristicas` (`id`, `posicion`, `pAtomico`, `nGrupo`, `nPeriodo`, `img`, `descrip`) VALUES
(1, 1, 1, 0, 1, 'ele001', 'Es el elemento químico de número atómico 1, representado por el símbolo H. Con una masa atómica de 1,00794 (7) u, es el más ligero de la tabla de los elementos. Por lo general, se presenta en su forma molecular, formando el gas diatómico H2 en condiciones normales. Este gas es inflamable, incoloro, inodoro, no metálico e insoluble en agua. El hidrógeno es el elemento químico más abundante, al constituir aproximadamente el 75 % de la materia visible del universo.2 nota 1 En su secuencia principal, las estrellas están compuestas principalmente por hidrógeno en estado de plasma. El hidrógeno elemental es relativamente raro en la Tierra y es producido industrialmente a partir de hidrocarburos como, por ejemplo, el metano. La mayor parte del hidrógeno elemental se obtiene in situ, es decir, en el lugar y en el momento en que se necesita. Los mayores mercados del mundo disfrutan de la utilización del hidrógeno para el mejoramiento de combustibles fósiles (en el proceso de hidrocraqueo) y en la producción de amoníaco (principalmente para el mercado de fertilizantes). El hidrógeno puede obtenerse a partir del agua por un proceso de electrólisis, pero resulta un método mucho más caro que la obtención a partir del gas natural.'),
(2, 2, 4, 18, 1, 'ele002', 'El helio (en griego:ἥλιος helios,sol) es un elemento químico de número atómico 2, símbolo He y peso atómico estándar de 4,0026. Pertenece al grupo 18 de la tabla periódica de los elementos, ya que al tener el nivel de energía completo presenta las propiedades de un gas noble. Es decir, es inerte (no reacciona) y al igual que estos, es un gas monoatómico incoloro e inodoro que cuenta con el menor punto de ebullición de todos los elementos químicos y solo puede ser licuado bajo presiones muy grandes y no puede ser congelado. Durante un eclipse solar en 1868, el astrónomo francés Pierre Janssen observó una línea espectral amarilla en la luz solar que hasta ese momento era desconocida. Norman Lockyer observó el mismo eclipse y propuso que dicha línea era producida por un nuevo elemento, al cual llamó helio, con lo cual, tanto a Lockyer como a Janssen se les adjudicó el descubrimiento de este elemento. En 1903 se encontraron grandes reservas de helio en campos de gas natural en los Estados Unidos, país con la mayor producción de helio en el mundo.'),
(3, 11, 23, 1, 3, 'ele003', '...'),
(4, 19, 39, 1, 4, 'ele004', '...'),
(5, 37, 85, 1, 5, 'ele005', '...'),
(6, 55, 133, 1, 6, 'ele006', '...'),
(7, 87, 223, 1, 7, 'ele007', '...'),
(8, 3, 7, 1, 2, 'ele008', '...'),
(9, 4, 9, 2, 2, 'ele00', '...'),
(10, 12, 24, 2, 3, 'ele00', '...'),
(11, 20, 40, 2, 4, 'ele00', '...'),
(12, 38, 87, 2, 5, 'ele00', '...'),
(13, 56, 137, 2, 6, 'ele00', '...'),
(14, 88, 226, 2, 7, 'ele00', '...');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `elementos`
--

CREATE TABLE `elementos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `simbolo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `caracteristicas_id` int(10) UNSIGNED NOT NULL,
  `estadosoxidacion_id` int(10) UNSIGNED NOT NULL,
  `grupos_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `elementos`
--

INSERT INTO `elementos` (`id`, `nombre`, `simbolo`, `caracteristicas_id`, `estadosoxidacion_id`, `grupos_id`) VALUES
(1, 'Hidrógeno', 'H', 1, 1, 1),
(2, 'Litio', 'Li', 3, 1, 1),
(3, 'Sodio', 'Na', 4, 1, 1),
(4, 'Potasio', 'K', 5, 1, 1),
(5, 'Rubidio', 'Rb', 6, 1, 1),
(6, 'Cesio', 'Cs', 7, 1, 1),
(7, 'Francio', 'Fr', 8, 1, 1),
(8, 'Berilio', 'Be', 9, 2, 2),
(9, 'Magnesio', 'Mg', 10, 2, 2),
(10, 'Calcio', 'Ca', 11, 2, 2),
(11, 'Estroncio', 'Sr', 12, 2, 2),
(12, 'Bario', 'Ba', 13, 2, 2),
(13, 'Radio', 'Ra', 14, 2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estadistica_juegos`
--

CREATE TABLE `estadistica_juegos` (
  `id` int(10) UNSIGNED NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `puntuacion` int(11) NOT NULL,
  `puntMax` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `juego_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `vecesJugado` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `estadistica_juegos`
--

INSERT INTO `estadistica_juegos` (`id`, `estado`, `puntuacion`, `puntMax`, `user_id`, `juego_id`, `created_at`, `updated_at`, `vecesJugado`) VALUES
(1, 0, 0, 0, 1, 1, NULL, NULL, NULL),
(2, 1, 0, 2, 24, 1, NULL, '2017-06-07 15:17:26', 2),
(3, 1, 288, 11308, 24, 2, NULL, '2017-06-07 15:16:35', 11),
(4, 0, 0, 0, 25, 1, NULL, NULL, NULL),
(5, 0, 0, 0, 25, 2, NULL, NULL, NULL),
(6, 0, 0, 0, 26, 1, '2017-05-24 19:27:38', '2017-05-24 19:27:38', NULL),
(7, 0, 0, 0, 26, 2, '2017-05-24 19:27:38', '2017-05-24 19:27:38', NULL),
(8, 0, 0, 0, 27, 1, '2017-05-24 19:27:58', '2017-05-24 19:27:58', NULL),
(9, 0, 0, 0, 27, 2, '2017-05-24 19:27:58', '2017-05-24 19:27:58', NULL),
(10, 0, 0, 0, 28, 1, '2017-05-24 19:53:12', '2017-05-24 19:53:12', NULL),
(11, 0, 0, 0, 28, 2, '2017-05-24 19:53:12', '2017-05-24 19:53:12', NULL),
(12, 0, 0, 0, 29, 1, '2017-05-24 19:54:43', '2017-05-24 19:54:43', NULL),
(13, 0, 0, 0, 29, 2, '2017-05-24 19:54:44', '2017-05-24 19:54:44', NULL),
(14, 0, 0, 0, 30, 1, '2017-05-24 19:55:41', '2017-05-24 19:55:41', NULL),
(15, 0, 0, 0, 30, 2, '2017-05-24 19:55:41', '2017-05-24 19:55:41', NULL),
(16, 0, 0, 0, 31, 1, '2017-05-24 19:56:12', '2017-05-24 19:56:12', NULL),
(17, 0, 0, 0, 31, 2, '2017-05-24 19:56:12', '2017-05-24 19:56:12', NULL),
(18, 0, 0, 0, 32, 1, '2017-05-24 20:01:22', '2017-05-24 20:01:22', NULL),
(19, 0, 0, 0, 32, 2, '2017-05-24 20:01:22', '2017-05-24 20:01:22', NULL),
(20, 1, 2, 14, 33, 1, '2017-05-24 20:02:30', '2017-05-31 14:28:26', 4),
(21, 1, 1688, 5504, 33, 2, '2017-05-24 20:02:31', '2017-05-28 14:43:40', 7),
(22, 0, 0, 0, 34, 1, '2017-05-24 20:04:09', '2017-05-24 20:04:09', NULL),
(23, 0, 0, 0, 34, 2, '2017-05-24 20:04:09', '2017-05-24 20:04:09', NULL),
(24, 0, 0, 0, 35, 1, '2017-05-24 20:04:30', '2017-05-24 20:04:30', NULL),
(25, 0, 0, 0, 35, 2, '2017-05-24 20:04:30', '2017-05-24 20:04:30', NULL),
(26, 0, 0, 0, 36, 1, '2017-05-24 20:04:50', '2017-05-24 20:04:50', NULL),
(27, 0, 0, 0, 36, 2, '2017-05-24 20:04:50', '2017-05-24 20:04:50', NULL),
(28, 0, 0, 0, 37, 1, '2017-05-24 20:05:58', '2017-05-24 20:05:58', NULL),
(29, 0, 0, 0, 37, 2, '2017-05-24 20:05:58', '2017-05-24 20:05:58', NULL),
(30, 0, 0, 0, 38, 1, '2017-05-24 20:06:36', '2017-05-24 20:06:36', NULL),
(31, 0, 0, 0, 38, 2, '2017-05-24 20:06:36', '2017-05-24 20:06:36', NULL),
(32, 0, 0, 0, 39, 1, '2017-05-24 20:12:08', '2017-05-24 20:12:08', NULL),
(33, 0, 0, 0, 39, 2, '2017-05-24 20:12:08', '2017-05-24 20:12:08', NULL),
(34, 1, 9, 9, 40, 1, '2017-06-04 16:28:53', '2017-06-05 16:13:30', 2),
(35, 1, 1460, 11308, 40, 2, '2017-06-04 16:28:53', '2017-06-05 16:15:53', 1),
(36, 0, 0, 0, 41, 1, '2017-06-11 07:26:53', '2017-06-11 07:26:53', 0),
(37, 0, 0, 0, 41, 2, '2017-06-11 07:26:53', '2017-06-11 07:26:53', 0),
(38, 1, 9, 54, 42, 1, '2017-06-11 07:31:25', '2017-06-18 13:13:30', 2),
(39, 0, 0, 0, 42, 2, '2017-06-11 07:31:25', '2017-06-11 07:31:25', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estadistica_practicas`
--

CREATE TABLE `estadistica_practicas` (
  `id` int(10) UNSIGNED NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `posicion` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `practicas_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `estadistica_practicas`
--

INSERT INTO `estadistica_practicas` (`id`, `estado`, `posicion`, `user_id`, `practicas_id`, `created_at`, `updated_at`) VALUES
(1, 0, '0', 39, 1, '2017-05-24 20:12:08', '2017-05-24 20:12:08'),
(2, 1, '#step-7', 40, 1, '2017-06-04 16:28:53', '2017-06-10 08:51:46'),
(3, 0, '#step-1', 40, 2, '2017-06-04 16:28:54', '2017-06-10 08:20:50'),
(4, 0, '#step-4', 42, 1, '2017-06-11 07:31:25', '2017-06-15 18:57:29'),
(5, 0, '#step-1', 42, 2, '2017-06-11 07:31:25', '2017-06-11 07:31:25'),
(6, 0, '#step-1', 42, 3, '2017-06-11 07:31:25', '2017-06-11 07:31:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estadosoxidacion`
--

CREATE TABLE `estadosoxidacion` (
  `id` int(10) UNSIGNED NOT NULL,
  `valor` int(10) UNSIGNED NOT NULL,
  `carga` enum('Anión','Catión') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Anión'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `estadosoxidacion`
--

INSERT INTO `estadosoxidacion` (`id`, `valor`, `carga`) VALUES
(1, 1, 'Catión'),
(2, 1, 'Catión'),
(3, 3, 'Catión'),
(4, 2, 'Catión'),
(4, 4, 'Catión'),
(5, 1, 'Catión'),
(5, 3, 'Catión'),
(5, 5, 'Catión'),
(6, 1, 'Catión'),
(6, 3, 'Catión'),
(6, 5, 'Catión'),
(6, 7, 'Catión');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupos`
--

CREATE TABLE `grupos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nGrupo` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descrip` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `grupos`
--

INSERT INTO `grupos` (`id`, `nGrupo`, `nombre`, `descrip`) VALUES
(1, 1, 'Alcalinos', 'El grupo de los metales alcalinos está formados por los elementos que se sitúan en la primera columna (grupo) de la tabla periódica o sistema periódico, lo componen los elementos químicos litio (Li), sodio (Na), potasio (K), rubidio (Rb), cesio (Cs), francio (Fr). Pertenecen a la serie de elementos s, esto significa que los metales alcalinos tienen su electrón más externo en un orbital s, cada uno tiene solo un electrón en su nivel energético más externo (s), con buena tendencia a perderlo (esto es debido a que tienen poca afinidad electrónica, y baja energía de ionización), con lo que forman un ion monovalente, M+. Esta configuración electrónica compartida da como resultado que tengan propiedades características muy similares. De hecho, los metales alcalinos proporcionan el mejor ejemplo de tendencias de grupo en propiedades en la tabla periódica, con elementos que exhiben un comportamiento homólogo bien caracterizado.'),
(2, 2, 'Alcalinotérreos', 'Los metales alcalinotérreos son un grupo de elementos que se encuentran situados en el grupo 2 de la tabla periódica y son los siguientes: berilio (Be), magnesio (Mg), calcio (Ca), estroncio (Sr), bario (Ba) y radio (Ra). Este último no siempre se considera, pues tiene un tiempo de vida media corta. El nombre «alcalinotérreos» proviene del nombre que recibían sus óxidos, «tierras», que tienen propiedades básicas (alcalinas). Poseen una electronegatividad ≤ 1,57 según la escala de Pauling. Características: son más duros que los metales alcalinos, tienen brillo y son buenos conductores eléctricos; menos reactivos que los alcalinos, buenos agentes reductores y forman compuestos iónicos. Todos ellos tienen dos (2) electrones en su capa más externa (electrones de Valencia).');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `juegos`
--

CREATE TABLE `juegos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dificultad` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `llamadaJuego` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descrip` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `juegos`
--

INSERT INTO `juegos` (`id`, `nombre`, `dificultad`, `llamadaJuego`, `descrip`) VALUES
(1, 'simonDice', 'media', 'simonDice', 'Descripcion simon dice'),
(2, '2048', 'baja', 'dosmil48', 'Descripcion 2048');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_04_23_170848_add_juegos_table', 1),
(4, '2017_04_23_170848_add_practicas_table', 1),
(5, '2017_04_23_171015_add_estadosoxidacion_table', 1),
(6, '2017_04_23_171936_add_caracteristicas_table', 1),
(7, '2017_04_23_172155_add_grupos_table', 1),
(8, '2017_04_23_180442_add_estadisticas_juegos_table', 1),
(9, '2017_04_23_180442_add_estadisticas_practicas_table', 1),
(10, '2017_04_23_182056_add_elementos_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(70) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `practicas`
--

CREATE TABLE `practicas` (
  `id` int(10) UNSIGNED NOT NULL,
  `descrip` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `grupo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `practicas`
--

INSERT INTO `practicas` (`id`, `descrip`, `grupo`) VALUES
(1, 'RepiteAlcalinos', '1'),
(2, 'RepiteAlcalinoterreos', '2'),
(3, 'RepiteHalogenos', '17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nick` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rol` enum('Alumno','Profesor') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Alumno',
  `email` varchar(70) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fechaNac` date DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `nombre`, `nick`, `password`, `rol`, `email`, `fechaNac`, `remember_token`, `created_at`, `updated_at`, `user_id`) VALUES
(1, '(sin profesor)', '(sin profesor)', '123456789', 'Profesor', 'bug@bug.com', '0000-00-00', NULL, NULL, NULL, NULL),
(3, ' ', 'nick', '$2y$10$tn1Jd6JjBFRXG9mHdU8WbuT49BKTXE14ua0uXO7/7j95RxGX0XswC', 'Alumno', 'ejemplo@ejemplo.com', NULL, NULL, '2017-05-09 20:05:05', '2017-05-09 20:05:05', 1),
(4, 'JohePrimer ElMeuFillNano', 'PP', '$2y$10$EPFKIYB7soRTnFIJnERFZu0A3hNjgBhclJN.Je0612VyLap5Emkv6', 'Alumno', 'pepe@pepe.com', '1980-10-12', 'ArstItYf8XWnBYLXXsdK1vIXgXpwFB7Jk9joYS5hWxDhd2UQuR6T3OIrpUnE', '2017-05-21 09:02:30', '2017-05-27 04:40:38', 1),
(5, NULL, 'fghfh', '$2y$10$CQ.mZTxqoH04G65nAx0VKe1IXeuc/db2CpeVIWrLNAntH9iKYhQnK', 'Profesor', 'teacher@teacher.com', NULL, '', '2017-05-21 12:26:40', '2017-05-21 12:26:40', 1),
(6, NULL, 'nickdfhgfhgd', '$2y$10$7xD.kexL1hEky1f6/nhduOtktbDZa8rvmSjjPhREzo8QysecKdHry', 'Alumno', '1@1.com', NULL, '', '2017-05-21 15:12:27', '2017-05-21 15:12:27', 1),
(7, NULL, 'nicketaer', '$2y$10$a1N01ZmDTQpMUAyg5/.Qce6zE1hZK53uDz9Eovul0mStvovYRpJbe', 'Profesor', 'bien@esta.com', NULL, NULL, '2017-05-21 15:16:55', '2017-05-21 15:16:55', 1),
(8, NULL, 'nick', '$2y$10$Yy9pg0UJShijhmxcfP95z.xa7XneQRSVT11.20ByoTofx/FFZJdTO', 'Alumno', 'ASDADA@ejemplo.com', NULL, NULL, '2017-05-21 15:54:50', '2017-05-21 15:54:50', 1),
(9, NULL, 'nick', '$2y$10$.ylTuXV1L.KNZISfVmS1deHvANpbOfSOTi51zJMLjrWsHgh82KYBa', 'Alumno', 'admin@admin.com', NULL, 'KpXeOWDKmRIntF3s4vQhdZsR5PlNOluwi3Z9g25dSwbQizFPC0WFEmsrv06S', '2017-05-21 15:57:19', '2017-05-21 15:57:19', 1),
(10, NULL, 'nick', '$2y$10$fwiZ9Pof2Mh6qu5TZs2M1ubViJNeaAV3rC4FLc6Z5cqAaxlpEp/Re', 'Profesor', 'platano_die@hotmail.com', NULL, NULL, '2017-05-21 16:00:52', '2017-05-21 16:00:52', 1),
(11, NULL, 'nick', '$2y$10$Qa8s1MZe..55zjIOlvqUsuwatCspTUuI8vGXFX8dRON7L1PZzSzoC', 'Profesor', '3332@gmail.com', NULL, 'Bkf7yyN2M4QFbvkgIv0mMbdpmUHMxvJOc5F6GmR2bgvgGFqkj934iHZXtTUD', '2017-05-21 16:02:45', '2017-05-21 16:02:45', 1),
(12, NULL, 'nick', '$2y$10$9S6gP6ZvGKYp/SyrtWVFkOJxTaef6kBCRFENSEZLnIKRq0ZMj4uOa', 'Alumno', '1@1.es', NULL, NULL, '2017-05-23 19:35:39', '2017-05-23 19:35:39', 1),
(13, NULL, 'nick', '$2y$10$oLBdOIovCXZvkitdXp/ZRefOx0QZp/yHhmZBKmeFMba4h9/I51kja', 'Profesor', '2@2.com', NULL, NULL, '2017-05-23 19:36:14', '2017-05-23 19:36:14', 1),
(14, NULL, 'nick', '$2y$10$6KCrooYrsEx7GOTA4h72SOYBGm5dtHM9fdjFKwTr6.InvkPLWFun6', 'Alumno', '2@3.com', NULL, NULL, '2017-05-23 19:44:18', '2017-05-23 19:44:18', 1),
(15, NULL, 'nick', '$2y$10$ZPqbyfv9SlhxuewigRiiROY9A9v93CJylVEnKZp4KIdbedaMYOza.', 'Alumno', '1@2.cpm', NULL, NULL, '2017-05-23 19:46:05', '2017-05-23 19:46:05', 1),
(16, NULL, 'nick', '$2y$10$wRlCUJCy1SCmOQI4h8/zdeM5gJ/q3aa78HVcAt0SVxF.6f4miVOMC', 'Alumno', '33333@hotmail.com', NULL, NULL, '2017-05-23 19:48:38', '2017-05-23 19:48:38', 24),
(17, NULL, 'nick', '$2y$10$2WfCc3GpykzJLjfANWPTf.KRGwZCYeAZC/ZkycUo4QhGagX7MPgxW', 'Alumno', '222@hotmail.com', NULL, NULL, '2017-05-23 20:01:41', '2017-05-23 20:01:41', 24),
(18, NULL, 'nick', '$2y$10$wKyUJPVfPR6QWmyRh.023.3c.Hi7pP.et332onaTBHJgWrgn/unJm', 'Profesor', '1111@111.com', NULL, NULL, '2017-05-23 20:03:59', '2017-05-23 20:03:59', 1),
(19, NULL, 'nick', '$2y$10$RjBITa5JEEZ2PKYJwDlOTerxIK3aMgX4jnceASgq3KtxbHXYbkr4i', 'Profesor', 'platano_die@43.com', NULL, NULL, '2017-05-23 20:07:49', '2017-05-23 20:07:49', 1),
(20, NULL, 'nick', '$2y$10$zKxiQKK/QG1CBVX8BDmzHucvvFxWhDUlG83PcrqKZHOhQsutUF7.6', 'Profesor', 'platano_die@423.com', NULL, NULL, '2017-05-23 20:09:14', '2017-05-23 20:09:14', 1),
(21, NULL, 'nick', '$2y$10$RAspf21m27SeJJGbR8B19elT9TtyEKMHsc6yEV55Svuf0DnRN/Xjy', 'Profesor', 'platano_die@42333.com', NULL, NULL, '2017-05-23 20:09:48', '2017-05-23 20:09:48', 1),
(22, NULL, 'nick', '$2y$10$eR4528WRQ/unNV0w5VMfDeaSt43nerF377GFwRcds7SndCR/D79tK', 'Profesor', 'platano_di3e@42333.com', NULL, NULL, '2017-05-23 20:10:38', '2017-05-23 20:10:38', 1),
(23, NULL, 'nick', '$2y$10$ffY5Q0t0U8DLm8abfVVxiuskSc4pKJNyGWiLDqjEy9rKodIhfSDtO', 'Alumno', 'szdzsdfzsd@admin.com', NULL, NULL, '2017-05-24 15:30:53', '2017-05-24 15:30:53', 24),
(24, NULL, 'nick', '$2y$10$AqqLdFPZZaCjhwuVMaAn2OlY70bTliXzPpHSHLP0c5ma87DOSaIUu', 'Profesor', '333ew@admin.com', NULL, '7slXBp5NXTTQ2yss0jsbE468QLQae2mDy47C0MvO4MPMqIEgaj1lPZ0O784k', '2017-05-24 15:44:00', '2017-05-24 15:44:00', 1),
(25, NULL, 'nick', '$2y$10$8uKLEhJluHGSI.ory02/6e42ii0VHyZWiL.mi.VYM2ZqGeYJfVta.', 'Profesor', 'r23rwefw@hotmail.com', NULL, 'YynerYWb86toK1Eux6B1SM7iyuKl6MXXmNFiJHsT5QyV0AJEhMkqAPaHhSIB', '2017-05-24 19:10:20', '2017-05-24 19:10:20', 1),
(26, NULL, 'nick', '$2y$10$muWgXAK2D/wJEg0I7X4sN.aAWaz0g5BV/5swViqoJRjsc0V606lwu', 'Profesor', 'asdda@admin.com', NULL, NULL, '2017-05-24 19:27:38', '2017-05-24 19:27:38', 1),
(27, NULL, 'nick', '$2y$10$6ChycRW9kzhqjz7x5GrVz.243NjEYa05.Ac4YWjNEOBkJjn9Hj6pa', 'Profesor', '22211@teacher.com', NULL, 'T689XJbPtz6o0qicYpOeR8JqGNYnqeW7uL1qKS2jCBhCOPUy2k0tjuTKAtlk', '2017-05-24 19:27:58', '2017-05-24 19:27:58', 1),
(28, NULL, 'nick', '$2y$10$gux9DJyvu5VdznEIsavgeu8PQtO03NkB87OtvZSwf2vpZuEpeDg4W', 'Profesor', 'sdsd@admin.com', NULL, NULL, '2017-05-24 19:53:11', '2017-05-24 19:53:11', 1),
(29, NULL, 'nick', '$2y$10$jb1qH2Mh/WT1RuwLzAiq7OW.PnwTfVDu7Xvv5GH3bHmD5vDFdceGe', 'Alumno', 'sdfsfsffd@hotmail.com', NULL, NULL, '2017-05-24 19:54:42', '2017-05-24 19:54:42', 24),
(30, NULL, 'nick', '$2y$10$KRfGmTgyWWwqHtNT41n1YefGSsYKQrMqZ7nbVPy/GUaRWsVLKtLb2', 'Profesor', 'asdasdadada@hotmail.com', NULL, NULL, '2017-05-24 19:55:41', '2017-05-24 19:55:41', 1),
(31, NULL, 'nick', '$2y$10$7azL0N24IDLxab0FwReGMeSbuq3tjsrqoaUCgG7Oa8sv88.FlxXSm', 'Profesor', 'asdasasd@hotmail.com', NULL, NULL, '2017-05-24 19:56:12', '2017-05-24 19:56:12', 1),
(32, NULL, 'nick', '$2y$10$ROe4/4U5I7Zw4NHhlMwR4OXYua6Qi6ZVNDewFKbHPci.2kuNjH6v6', 'Profesor', 'asdasdad@hotmail.com', NULL, NULL, '2017-05-24 20:01:22', '2017-05-24 20:01:22', 1),
(33, NULL, 'nick', '$2y$10$.AmSZFrBjKzqHS7T7/xTnOZCuNeF9tWyNx8dCH7QQDD0YQNeMp2kC', 'Alumno', 'asdadad@hotmail.com', '1980-10-12', '1KL75i0nAGtb5zX84VDjHGPUWhzx8vvGjVo4zzdghlJqCxyA7E4GhJTFY2oD', '2017-05-24 20:02:30', '2017-06-03 09:06:28', 24),
(34, NULL, 'nick', '$2y$10$OyPtCZFkaCWu.BtfeRJ.Pu.xoOD8vqcNoGLnmEKX6pakLl1J/.twe', 'Profesor', '2111@hotmail.com', NULL, NULL, '2017-05-24 20:04:09', '2017-05-24 20:04:09', 1),
(35, NULL, 'nick', '$2y$10$APkSpidswGLOoctLZw5n0eO8Yi/jRMWkyCUlGWxNmFc44NeJUmcj2', 'Profesor', 'qqqq@hotmail.com', NULL, NULL, '2017-05-24 20:04:30', '2017-05-24 20:04:30', 1),
(36, NULL, 'nick', '$2y$10$7qk/wEumNX5wQI5TOuJGb.Is8R5x671bnbrg.dv9bZeTFsG1BTPfO', 'Profesor', 'asdadad@asdada.com', NULL, NULL, '2017-05-24 20:04:50', '2017-05-24 20:04:50', 1),
(37, NULL, 'nick', '$2y$10$.sMpoYGziTda2jks2a60/eAUc01T.siwAyhtlkCVCBhYCOR.bmpRO', 'Profesor', 'asdada22@asdada.com', NULL, NULL, '2017-05-24 20:05:58', '2017-05-24 20:05:58', 1),
(38, NULL, 'nick', '$2y$10$MalDZ.b3hWg.YiDLs2dVEOnAz4U3IRvdw2HgouY4LrsyjvoLwdbS2', 'Profesor', 'qweqweq@hotmail.com', NULL, NULL, '2017-05-24 20:06:36', '2017-05-24 20:06:36', 1),
(39, NULL, 'nick', '$2y$10$ds0twSxpzmyehqh8pY5DSOQV6pJ7fdHfEegaOIOFSjzu4yd31Sw5.', 'Profesor', 'c@2.cpm', NULL, 'hw6YNW67HioZgU8I9QU6lSlMtabZ8y5xXxeAFLoodu4WQyhck8iFZwZjFGCt', '2017-05-24 20:12:08', '2017-05-24 20:12:08', 1),
(40, NULL, 'nick', '$2y$10$B5.FIljv0KtN7fQMVZcj7u.LyuhVYrWd8VGb5BwXh.wxXtVlmObn6', 'Alumno', '24@24.es', '1980-10-12', 'eox7ssYbS1SsfcLz9g15bEpCaIQJIdZH3sUW6MJwVWa5SnoAwuOdG2RTNs7E', '2017-06-04 16:28:53', '2017-06-04 16:29:21', 24),
(41, NULL, 'nick', '$2y$10$xr8Zu5Fh0aItEAc790WwTeFyk.w1UNx3KGtVPTcZ4wpMc9EfAZJ.m', 'Profesor', 'bbb@bbb.com', NULL, NULL, '2017-06-11 07:26:53', '2017-06-11 07:26:53', 1),
(42, NULL, 'nick', '$2y$10$cf/Pev7ndIjk0KjVgebnuOnb1ePaP0OKyQnwrS2m1Y1QF3jWH3k6C', 'Alumno', 'bbb1@bbb1.com', NULL, 'mkR64061ReBKLaumUNpr1ZQbgeozYrCu781m9A7buk6RBrABpuf7gFVzkwFT', '2017-06-11 07:31:25', '2017-06-11 07:31:25', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `caracteristicas`
--
ALTER TABLE `caracteristicas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `elementos`
--
ALTER TABLE `elementos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `elementos_caracteristicas_id_foreign` (`caracteristicas_id`),
  ADD KEY `elementos_estadosoxidacion_id_foreign` (`estadosoxidacion_id`),
  ADD KEY `elementos_grupos_id_foreign` (`grupos_id`);

--
-- Indices de la tabla `estadistica_juegos`
--
ALTER TABLE `estadistica_juegos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `estadistica_juegos_user_id_foreign` (`user_id`),
  ADD KEY `estadistica_juegos_juego_id_foreign` (`juego_id`);

--
-- Indices de la tabla `estadistica_practicas`
--
ALTER TABLE `estadistica_practicas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `estadistica_practicas_user_id_foreign` (`user_id`),
  ADD KEY `estadistica_practicas_practicas_id_foreign` (`practicas_id`);

--
-- Indices de la tabla `estadosoxidacion`
--
ALTER TABLE `estadosoxidacion`
  ADD PRIMARY KEY (`id`,`valor`);

--
-- Indices de la tabla `grupos`
--
ALTER TABLE `grupos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `juegos`
--
ALTER TABLE `juegos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `practicas`
--
ALTER TABLE `practicas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `caracteristicas`
--
ALTER TABLE `caracteristicas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `elementos`
--
ALTER TABLE `elementos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `estadistica_juegos`
--
ALTER TABLE `estadistica_juegos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT de la tabla `estadistica_practicas`
--
ALTER TABLE `estadistica_practicas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `grupos`
--
ALTER TABLE `grupos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `juegos`
--
ALTER TABLE `juegos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `practicas`
--
ALTER TABLE `practicas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `elementos`
--
ALTER TABLE `elementos`
  ADD CONSTRAINT `elementos_caracteristicas_id_foreign` FOREIGN KEY (`caracteristicas_id`) REFERENCES `caracteristicas` (`id`),
  ADD CONSTRAINT `elementos_estadosoxidacion_id_foreign` FOREIGN KEY (`estadosoxidacion_id`) REFERENCES `estadosoxidacion` (`id`),
  ADD CONSTRAINT `elementos_grupos_id_foreign` FOREIGN KEY (`grupos_id`) REFERENCES `grupos` (`id`);

--
-- Filtros para la tabla `estadistica_juegos`
--
ALTER TABLE `estadistica_juegos`
  ADD CONSTRAINT `estadistica_juegos_juego_id_foreign` FOREIGN KEY (`juego_id`) REFERENCES `juegos` (`id`),
  ADD CONSTRAINT `estadistica_juegos_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `estadistica_practicas`
--
ALTER TABLE `estadistica_practicas`
  ADD CONSTRAINT `estadistica_practicas_practicas_id_foreign` FOREIGN KEY (`practicas_id`) REFERENCES `practicas` (`id`),
  ADD CONSTRAINT `estadistica_practicas_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
