var rutaRelativa = $('#inicio').attr('href');
var elementosJSON = []
var elementoGrupo = 0;

function recuperaElementos(arrayElementos, grupoEscogido) {
    console.log(arrayElementos[8].grupo);

    for (var j = 0; j<arrayElementos.length; j++) {
        if (arrayElementos[j].grupo == grupoEscogido) {
            console.log(j);
            elementosJSON.push(arrayElementos[j]);
        }
    }

    for (var i = 1; i<=elementosJSON.length; i++) { 

        $("#paso"+i).text(elementosJSON[i-1].simbolo);
        $("#small"+i).text(elementosJSON[i-1].nombreElemento);

        $("#imagen"+i).attr('src', rutaRelativa+"/images/"+elementosJSON[i-1].simbolo+".png");
        $("#boton"+i).attr('data-elemento', elementosJSON[i-1].simbolo);
        $("#audio"+i).attr('data-elemento', elementosJSON[i-1].simbolo);

        $("#boton"+i).text(elementosJSON[i-1].nombreElemento);

        $("#boton"+i).unbind("click", function() {});

        $("#boton"+i).on("click", function() {
            var audioEl = $(this).attr('data-elemento');
            $("audio[data-elemento="+$(this).attr('data-elemento')+"]").attr('src', rutaRelativa+"/audios/"+audioEl+".mp3");
            $("audio[data-elemento="+$(this).attr('data-elemento')+"]").get(0).play();
        });
    }
    elementosJSON = []
}

function recuperarPosicionPractica(estadisticasPractica, grupo) {
    console.log(estadisticasPractica);
    var posicion = "step-1";
    for (var i = 0; i<estadisticasPractica.length; i++) { 
        console.log(estadisticasPractica[i].grupo);
        if (parseInt(estadisticasPractica[i].grupo) === grupo) {
            if (estadisticasPractica[i].posicion !== "") {
                location.hash = estadisticasPractica[i].posicion;
                return;
            } else {
                location.hash = posicion;
            }
            location.hash = estadisticasPractica[i].posicion;
        }
    }

    
    }
