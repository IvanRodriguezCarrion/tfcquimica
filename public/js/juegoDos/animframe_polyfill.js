/*
    Mediante la función requestAnimationFrame le decimos al navegador que vamos a llevar a cabo
    una animación y que se prepare para repintar en el siguiente ciclo de animación.
*/
(function() {
  var lastTime = 0;
  var vendors = ['webkit', 'moz'];
  for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
    window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
    window.cancelAnimationFrame =
    window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
  }
/*
    Si la request que le hemos hecho al navegador no se ha completado o ha devuelto error vuelve a solicitarla
    con un tiempo de retraso definido dentro del setTimeOut.
*/
  if (!window.requestAnimationFrame) {
    window.requestAnimationFrame = function(callback, element) {
      var currTime = new Date().getTime();
      var timeToCall = Math.max(0, 16 - (currTime - lastTime));
      var id = window.setTimeout(function() { callback(currTime + timeToCall); },
      timeToCall);
      lastTime = currTime + timeToCall;
      return id;
    };
  }
/*
    Si no se ha detenido el ciclo de animación se define la función de detención de la misma.
*/
  if (!window.cancelAnimationFrame) {
    window.cancelAnimationFrame = function(id) {
      clearTimeout(id);
    };
  }
}());
