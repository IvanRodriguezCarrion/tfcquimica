window.requestAnimationFrame(function () {
    var game = new GameManager(4, KeyboardInputManager, HTMLActuator, LocalScoreManager);
    game.actuator.addTile = function (tile) {
        var self = this;
        var wrapper   = document.createElement("div");
        var inner     = document.createElement("div");
        var M1elementosDeHaMg = ["H", "He", "Li", "Be", "B", "C", "N", "O", "F", "Ne", "Na","Mg"];
        var M1elementosDeAlaCr = ["Al", "Si", "P", "S", "Cl", "Ar", "K", "Ca", "Sc", "Ti", "V", "Cr"];
        var elementos = M1elementosDeHaMg;
        var position  = tile.previousPosition || { x: tile.x, y: tile.y };
        positionClass = this.positionClass(position);
        var classes = ["tile", "tile-" + tile.value, positionClass];
        this.applyClasses(wrapper, classes);
        inner.classList.add("tile-inner");
        switch (tile.value) {
            case 2:
                inner.textContent = elementos[0];
                break;
            case 4:
                inner.textContent = elementos[1];
                break;
            case 8:
                inner.textContent = elementos[2];
                break;
            case 16:
                inner.textContent = elementos[3];
                break;
            case 32:
                inner.textContent = elementos[4];
                break;
            case 64:
                inner.textContent = elementos[5];
                break;
            case 128:
                inner.textContent = elementos[6];
                break;
            case 256:
                inner.textContent = elementos[7];
                break;
            case 512:
                inner.textContent = elementos[8];
                break;
            case 1024:
                inner.textContent = elementos[9];
                break;
            case 2048:
                inner.textContent = elementos[10];
                break;
            case 4096:
                inner.textContent = elementos[11];
                break;
        }

        if (tile.previousPosition) {
            window.requestAnimationFrame(function () {
                classes[2] = self.positionClass({ x: tile.x, y: tile.y });
                self.applyClasses(wrapper, classes);
            });
        } else if (tile.mergedFrom) {
            classes.push("tile-merged");
            this.applyClasses(wrapper, classes);
            tile.mergedFrom.forEach(function (merged) {
                self.addTile(merged);
            });
        } else {
            classes.push("tile-new");
            this.applyClasses(wrapper, classes);
        }
        wrapper.appendChild(inner);
        this.tileContainer.appendChild(wrapper);
    };
    game.restart();
});
