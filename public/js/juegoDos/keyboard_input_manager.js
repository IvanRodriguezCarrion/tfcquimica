/*
    Aquí tratamos con los eventos de teclado, sobreescribiendo muchos de los controles predefinidos por
    las librerías de JS
*/

function KeyboardInputManager() {
    this.events = {};

    this.listen();
}

/*
    Se recogen los distintos tipos de eventos que hay (descartando los ya existentes) y
    se les asocia la llamada dentro de cada evento.
*/

KeyboardInputManager.prototype.on = function (event, callback) {
    if (!this.events[event]) {
        this.events[event] = [];
    }
    this.events[event].push(callback);
};

/*
    "emit" hace referencia a lo que emiten los eventos para que sean escuchados por los distintos listeners
    con lo que aquí, a cad uno de los eventos dentro del vector eventos se le añade todas las llamadas 
    que se le pasen por callback.
*/

KeyboardInputManager.prototype.emit = function (event, data) {
    var callbacks = this.events[event];
    if (callbacks) {
        callbacks.forEach(function (callback) {
            callback(data);
        });
    }
};

/*
    Se definen los listen a cada unas de las teclas que se van a usar.
*/

KeyboardInputManager.prototype.listen = function () {
    var self = this;

    var map = {
        38: 0, // Up
        39: 1, // Right
        40: 2, // Down
        37: 3, // Left
        75: 0, // vim keybindings
        76: 1,
        74: 2,
        72: 3,
        87: 0, // W
        68: 1, // D
        83: 2, // S
        65: 3  // A
    };
    
    /*
        Se añaden los listeners a las teclas mappeadas anteriormente. Si alguno de los eventos
        afecta a las teclas predefinidas en los modifers entonces no se asocia el evento de emitir
        el "move" de cada tecla.
    */
    
    document.addEventListener("keydown", function (event) {
        var modifiers = event.altKey || event.ctrlKey || event.metaKey ||
            event.shiftKey;
        var mapped    = map[event.which];

        if (!modifiers) {
            if (mapped !== undefined) {
                event.preventDefault();
                self.emit("move", mapped);
            }
            //con el 32 hace referencia a la tecla espacio y sirve para reiniciar el juego.
            if (event.which === 32) self.restart.bind(self)(event);
        }
    });
    
    /*
        Eventos relacionados con la caja del juego
    */
    
    var retry = document.getElementsByClassName("retry-button")[0];
    retry.addEventListener("click", this.restart.bind(this));
    retry.addEventListener("touchend", this.restart.bind(this));

    // Los liseners para los eventos de movimiento de las celdas por la caja
    
    var touchStartClientX, touchStartClientY;
    var gameContainer = document.getElementsByClassName("game-container")[0];

    gameContainer.addEventListener("touchstart", function (event) {
        if (event.touches.length > 1) return;

        touchStartClientX = event.touches[0].clientX;
        touchStartClientY = event.touches[0].clientY;
        event.preventDefault();
    });

    gameContainer.addEventListener("touchmove", function (event) {
        event.preventDefault();
    });

    gameContainer.addEventListener("touchend", function (event) {
        if (event.touches.length > 0) return;

        var dx = event.changedTouches[0].clientX - touchStartClientX;
        var absDx = Math.abs(dx);

        var dy = event.changedTouches[0].clientY - touchStartClientY;
        var absDy = Math.abs(dy);

        if (Math.max(absDx, absDy) > 10) {
            // (right : left) : (down : up)
            self.emit("move", absDx > absDy ? (dx > 0 ? 1 : 3) : (dy > 0 ? 2 : 0));
        }
    });
};

KeyboardInputManager.prototype.restart = function (event) {
    event.preventDefault();
    this.emit("restart");
};
