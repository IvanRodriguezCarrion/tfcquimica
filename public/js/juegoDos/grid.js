//Tamaño de la grid de juego, que viene definida por lo que se defina en la configuración del juego.

function Grid(size) {
    this.size = size;

    this.cells = [];

    this.build();
}

// Constructor de celdas.
Grid.prototype.build = function () {
    for (var x = 0; x < this.size; x++) {
        var row = this.cells[x] = [];

        for (var y = 0; y < this.size; y++) {
            row.push(null);
        }
    }
};

// Encuentra la primera posición libre aleatoriamente
Grid.prototype.randomAvailableCell = function () {
    var cells = this.availableCells();

    if (cells.length) {
        return cells[Math.floor(Math.random() * cells.length)];
    }
};

Grid.prototype.availableCells = function () {
    var cells = [];

    this.eachCell(function (x, y, tile) {
        if (!tile) {
            cells.push({ x: x, y: y });
        }
    });

    return cells;
};

// llamadas a cada una de las celdas
Grid.prototype.eachCell = function (callback) {
    for (var x = 0; x < this.size; x++) {
        for (var y = 0; y < this.size; y++) {
            callback(x, y, this.cells[x][y]);
        }
    }
};

// La admiración hace que la expresión se convierta en una expresión de función. 
// La doble !! permite que  la llamada a la función se convierta en una expresión TRUE o FALSE.
Grid.prototype.cellsAvailable = function () {
    return !!this.availableCells().length;
};

// Sistema para controlar si una celda está ocupada. La ! hace que se devuelva el booleano invertido
Grid.prototype.cellAvailable = function (cell) {
    return !this.cellOccupied(cell);
};
// Sumada a la de arriba, si esta expresión devuelve true significa que la celda NO está disponible y se
//devuelve como false.
Grid.prototype.cellOccupied = function (cell) {
    return !!this.cellContent(cell);
};
// Le asigna a cada una de las posiciones su contenido enlazado
Grid.prototype.cellContent = function (cell) {
    if (this.withinBounds(cell)) {
        return this.cells[cell.x][cell.y];
    } else {
        return null;
    }
};

// Inserts a tile at its position
Grid.prototype.insertTile = function (tile) {
    this.cells[tile.x][tile.y] = tile;
};

Grid.prototype.removeTile = function (tile) {
    this.cells[tile.x][tile.y] = null;
};

Grid.prototype.withinBounds = function (position) {
    return position.x >= 0 && position.x < this.size &&
        position.y >= 0 && position.y < this.size;
};
