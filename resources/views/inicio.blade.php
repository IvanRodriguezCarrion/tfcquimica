@extends('layoutTemplate')

@section('titulo', "¡Bienvenido a EduQuѥm!")

@section('contenido')
<section>
    <div id="head">
        <div class="line">
            <h1>¡Bienvenido a EduQuѥm $^$!</h1>
        </div>
    </div>
    <div id="content">
        <div class="line">
            <div id="carousel">
                <div class="line">
                    <div class=" lg-offset-4 lg-8" >
                        <div id = "intro">


                        </div>
                    </div>
                </div>

                <div id="first-block">
                    <div class="line">
                        <h2>¡APRENDE JUGANDO!</h2>
                        <p class="subtitile"><em>Aquello que divierte, enseña</em> 
                        </p>
                        <div class="margin">
                            <div class="s-12 m-6 l-3 margin-bottom">
                                <i class="icon-paperplane_ico icon2x"></i>
                                <h3>Sobre mi</h3>
                                <p>Licenciado en Químicas, protodocente e informático (por este orden) hacen que me interese cubrir partes de la química que a mi me resultaron complejas y siempre teniendo en la cabeza el «ojalá hubiese tenido esta herramienta cuando estudiaba»
                                </p>
                            </div>
                            <div class="s-12 m-6 l-3 margin-bottom">
                                <i class="icon-star icon2x"></i>
                                <h3>Metología</h3>
                                <p>Repetir, repetir, repetir y repetir. Hay cosas en ciencia que se deben saber porque, básicamente, son así. No tienen una explicación por detrás ¿Por qué el sodio tiene el símbolo de Na? Pues porque la palabra «nadium» (en latín) evolucionó a sodio y nos adaptamos a lo que una organización (IUPAC) dijo.
                                </p>
                            </div>
                            <div class="s-12 m-6 l-3 margin-bottom">
                                <i class="icon-message icon2x"></i>
                                <h3>Herramientas</h3>
                                <p>Juegos y más juegos. Gracias a la tecnología <a href = "https://phaser.io/">phaser</a> he desarrollado y readaptado juegos para la temática que me interesa enseñar. Espero en un futuro poder ir ampliando y variando la temática. Todos los resultados obtenidos en los juegos se registran y podrás verlo en tu perfil.
                                </p>
                            </div>
                            <div class="s-12 m-6 l-3 margin-bottom">
                                <i class="icon-mail icon2x"></i>
                                <h3>Profesores</h3>
                                <p>Vosotros tambien tenéis vuestra parte dentro de este plan. Dentro de vuestro perfil podréis vuestras estadísdiscas (si queréis jugar) y, además, las de vuestros alumnos que hayan introducido tu código personal y único en su sección del perfil. 
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div id="second-block">
            <div class="line">
               <div class="margin-bottom">
                  <div class="margin">
                     <article class="s-12 l-8 center">
                        <h1>Amazing title</h1>
                        <p class="margin-bottom">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
                        </p>
                        <a class="button s-12 l-4 center" href="product.html">Read more</a>  			
                     </article>
                  </div>
               </div>
            </div>
         </div>
        </section>
    <script src="{{ asset('js/phaser.min.js') }}" type="text/javascript"></script>
    <script>
        var game = new Phaser.Game(980, 551, Phaser.AUTO, 'intro', { preload: preload, create: create });

        var content = [
            "  ",
            "Hola, ",
            "quizá no te guste",
            " pero la Química lo es todo.",
            " ",
            "Quizá no te interese",
            "...y lo entiendo.",
            "Pero estoy aquí para ayudarte.",
            "...",
            "No desistas",
            "prueba a jugar aquí, ", 
            "quizá te sea más fácil así.",
        ];

        var text;
        var index = 0;
        var line = '';

        function preload() {
            game.load.image('cod', '{{asset('images/imagenFondoIntro.png') }}');
        }

        function create() {

            game.add.sprite(0, 0, 'cod');

            text = game.add.text( 32, 450, '', { font: "25pt Courier", fill: "#19cb65", stroke: "#119f4e", strokeThickness: 2, boundsAlignH:"left", boundsAlignV:"bottom" });

            nextLine();

        }

        function updateLine() {

            if (line.length < content[index].length)
            {
                line = content[index].substr(0, line.length + 1);
                // text.text = line;
                text.setText(line);
            }
            else
            {
                //  Wait 2 seconds then start a new line
                game.time.events.add(Phaser.Timer.SECOND * 2, nextLine, this);
            }

        }

        function nextLine() {

            index++;

            if (index < content.length)
            {
                line = '';
                game.time.events.repeat(80, content[index].length + 1, updateLine, this);
            }

        }
    </script>
    @endsection