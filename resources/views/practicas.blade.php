@extends('layoutTemplate')

@section('titulo', "¡A practicar!")

@section('contenido')
<!-- div que contendrá aquello que se mostrará. Unos cuantos botones para que sea sencillo de escoger.-->

<section>
    <div id="head">
        <div class="line">
            <h1>¡Clicka en el grupo que quieras y aprende!</h1>
        </div>
    </div>
    <div id="content">
        <div class="line">
            <div class="margin">
                <div class="s-12 m-6 l-4">
                    <div class="content-block margin-bottom">
                        <i class="icon-paperplane_ico icon2x"></i>
                        <h3>Grupo 1</h3>
                        <p>Metales brillantes, suaves y muy reactivos al aire. ¡Reaccionan incluso con el agua del ambiente!
                        </p>

                        <button type="button" id="grupo1" name = "grupo" class="btn btn-primary btn-lg raised" data-toggle="modal" data-target=".bd-example-modal-lg">¡Alcalinos!</button>

                    </div>
                </div>
                <div class="s-12 m-6 l-4">
                    <div class="content-block margin-bottom">
                        <i class="icon-display_screen icon2x"></i>
                        <h3>Grupo 2</h3>
                        <p>Metales brillantes, coloreados y blandos. Se pueden cortar hasta con un cuchillo!
                        </p>
                        <button type="button" id="grupo2" name = "grupo" class="btn btn-primary btn-lg raised" data-toggle="modal" data-target=".bd-example-modal-lg">¡Alcalinotérreos!</button>
                    </div>
                </div>
                <div class="s-12 m-6 l-4">
                    <div class="content-block margin-bottom">
                        <i class="icon-heart icon2x"></i>
                        <h3>Grupo 17</h3>
                        <p>Gases de colores y corrosivos ¡No respires sus nubes de colores!
                        </p>
                        <button type="button" id="grupo17" name = "grupo" class="btn btn-primary btn-lg raised" data-toggle="modal" data-target=".bd-example-modal-lg">¡Halógenos*!</button>
                    </div>
                </div>
                <div class="s-12 m-6 l-4">
                    <div class="content-block margin-bottom">
                        <i class="icon-attachment icon2x"></i>
                        <h3>Grupo 16</h3>
                        <p>¡Oxígeno que respiras, azufre amarillo en las calles y selenio en tu champú!
                        </p>
                        <button type="button" id="grupo16" name = "grupo" class="btn btn-primary btn-lg raised" data-toggle="modal" data-target=".bd-example-modal-lg">¡Calcógenos*!</button>
                    </div>
                </div>
                <div class="s-12 m-6 l-4">
                    <div class="content-block margin-bottom">
                        <i class="icon-reorder icon2x"></i>
                        <h3>Grupo 15</h3>
                        <p>¡El amoniaco apesta y los demás tienen multiples formas y no me aclaro!
                        </p>
                        <button type="button" id="grupo15" name = "grupo" class="btn btn-primary btn-lg raised" data-toggle="modal" data-target=".bd-example-modal-lg">¡Nitrogenoides*!</button>
                    </div>
                </div>
                <div class="s-12 m-6 l-4">
                    <div class="content-block margin-bottom">
                        <i class="icon-mail icon2x"></i>
                        <h3>Grupo 14</h3>
                        <p>Del carbono viene la vida y del silicio la tecnología. Del resto, ni idea :)
                        </p>
                        <button type="button" id="grupo14" name = "grupo" class="btn btn-primary btn-lg raised" data-toggle="modal" data-target=".bd-example-modal-lg">¡Carbonoideos*!</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<div class="text-center">
    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="practicaGruposlLabel">Escucha, lee, practica y ¡aprende!</h4>
                </div>
                <div class="">

                    <!-- SmartWizard html -->
                    <div id="smartwizard">
                        <ul id = "campos">
                            <li><a href="#step-1"><strong id="paso1"></strong><br /><small id="small1"></small></a></li>
                            <li><a href="#step-2"><strong id="paso2"></strong><br /><small id="small2"></small></a></li>
                            <li><a href="#step-3"><strong id="paso3"></strong><br /><small id="small3"></small></a></li>
                            <li><a href="#step-4"><strong id="paso4"></strong><br /><small id="small4"></small></a></li>
                            <li><a href="#step-5"><strong id="paso5"></strong><br /><small id="small5"></small></a></li>
                            <li><a href="#step-6"><strong id="paso6"></strong><br /><small id="small6"></small></a></li> 
                            <li><a href="#step-7"><strong id="paso7">Meta</strong><br /><small id="small7">¡Final!</small></a></li> 
                        </ul>

                        <div id="pasos">
                            <div id="step-1" class="">
                                <div id="contenido1" class = "row">
                                    <div class = "col-md-12 align-middle text-center">
                                        <div class="col-md-6 col-md-offset-3">
                                            <img id="imagen1" class="img-responsive center-block" src=''/>
                                            <button id='boton1' data-elemento = "" type='button' class='btn btn-warning ' name = "elemento"></button></div>
                                    </div>
                                    <audio class='hidden' id='audio1' src=' ' data-elemento = ""><p>Tu navegador no implementa el elemento audio.</p></audio>
                                </div>
                            </div>
                            <div id="step-2" class="">
                                <div id="contenido1" class = "row">
                                    <div class = "col-md-12 align-middle text-center">
                                        <div class = "col-md-6 col-md-offset-3 ">
                                            <img id="imagen2" class="img-responsive center-block" src=''/>
                                            <button id='boton2' data-elemento = "" type='button' class='btn btn-warning' name = "elemento"></button>
                                        </div>
                                    </div>
                                    <audio class='hidden' id='audio2' src=' ' data-elemento = ""><p>Tu navegador no implementa el elemento audio.</p></audio>
                                </div>
                            </div>
                            <div id="step-3" class="">
                                <div id="contenido1" class = "row">
                                    <div class = "col-md-12 align-middle text-center">
                                        <div class = "col-md-6 col-md-offset-3">
                                            <img id="imagen3" class="img-responsive center-block" src=''/>
                                            <button id='boton3' data-elemento = "" type='button' class='btn btn-warning' name = "elemento"></button>
                                        </div>
                                    </div>
                                    <audio class='hidden' id='audio3' src=' ' data-elemento = ""><p>Tu navegador no implementa el elemento audio.</p></audio>
                                </div>
                            </div>
                            <div id="step-4" class="">
                                <div id="contenido1" class = "row">
                                    <div class = "col-md-12 align-middle text-center">
                                        <div class = "col-md-6 col-md-offset-3">
                                            <img id="imagen4" class="img-responsive center-block" src=''/>
                                            <button id='boton4' data-elemento = "" type='button' class='btn btn-warning' name = "elemento"></button>
                                        </div>
                                        <audio class='hidden' id='audio4' src=' ' data-elemento = ""><p>Tu navegador no implementa el elemento audio.</p></audio>
                                    </div>
                                </div>
                            </div>
                            <div id="step-5" class="">
                                <div id="contenido1" class = "row">
                                    <div class = "col-md-12 align-middle text-center">
                                        <div class = "col-md-6 col-md-offset-3">
                                            <img id="imagen5" class="img-responsive center-block" src=''/>
                                            <button id='boton5' data-elemento = "" type='button' class='btn btn-warning' name = "elemento"></button>
                                        </div>
                                        <audio class='hidden' id='audio5' src=' ' data-elemento = ""><p>Tu navegador no implementa el elemento audio.</p></audio>
                                    </div>
                                </div>
                            </div>
                            <div id="step-6" class="">
                                <div id="contenido1" class = "row">
                                    <div class = "col-md-12 align-middle text-center">
                                        <div class = "col-md-6 col-md-offset-3">
                                            <img id="imagen6" class="img-responsive center-block" src=''/>
                                            <button id='boton6' data-elemento = "" type='button' class='btn btn-warning' name = "elemento"></button>
                                        </div>
                                        <audio class='hidden' id='audio6' src=' ' data-elemento = ""><p>Tu navegador no implementa el elemento audio.</p></audio>
                                    </div>
                                </div>
                            </div>
                            <div id="step-7" class="">
                                <div id="contenido1" class = "row">
                                    <div class = "col-md-6 col-md-offset-3">
                                        ¡Has conseguido alcanzar el final! Dale a guardar para guardar tu avance. Ahora sabes un poco más sobre este conjunto de elementos. Haz tantas pasadas como quieras y te será más fácil recordarlos en el futuro.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="grupoOculto" value="">
</div>
<!-- Include SmartWizard JavaScript source -->
<script src="{{ asset('js/smartWizard.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/scripts.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    var grupo = 0;
    $(document).ready(function(){
        $("button[name=grupo]").on("click", function(e) {
            console.log($(this).attr("id"));
            var grupete = $(this).attr("id");
            if (grupete == "grupo1")  {
                grupo = 1;
            } else if (grupete == "grupo2") {
                grupo = 2;
            };
            $.ajax({
                type: 'get',
                url: 'practicas/grupo',
                dataType: 'json',
                success: function (data) { 
                    recuperaElementos(data, grupo);
                },
                error: function () {
                    alert('error');
                }
            });

            $.ajax({
                type: 'get',
                url: 'practicas/posicion',
                dataType: 'json',
                success: function (data1) { 
                    recuperarPosicionPractica(data1, grupo);
                },
                error: function () {
                    //alert("error");
                }
            });

            $("#grupoOculto").val(grupo);
            console.log("GRIPO: "+$("#grupoOculto").val());

        });

        //La función recuperarElemento está dentro de script.js

        // Step show event 
        $("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection, stepPosition) {
            //alert("You are on step "+stepNumber+" now");
            if(stepPosition === 'first'){
                $("#prev-btn").addClass('disabled');
            }else if(stepPosition === 'final'){
                $("#next-btn").addClass('disabled');
            }else{
                $("#prev-btn").removeClass('disabled');
                $("#next-btn").removeClass('disabled');
            }
        });

        // Toolbar extra buttons
        var btnFinish = $('<button></button>').text('Guardar')
        .addClass('btn btn-info')
        .on('click', function(){ 
            var idUsuario = {{session()->get('usuarioId')}};
            var posicion = location.hash;
            var grupo = $("#grupoOculto").val();
        guardarResultado(idUsuario, posicion, grupo)}
                     );
    var btnCancel = $('<button></button>').text('Reiniciar')
    .addClass('btn btn-danger')
    .on('click', function(){ $('#smartwizard').smartWizard("reset"); });                         


    // Smart Wizard
    $('#smartwizard').smartWizard({ 
        selected: 0, 
        theme: 'arrows',
        transitionEffect:'fade',
        showStepURLhash: true,
        toolbarSettings: {toolbarPosition: 'bottom',
                          toolbarExtraButtons: [btnFinish, btnCancel]
                         }
    });


    // External Button Events
    $("#reset-btn").on("click", function() {
        // Reset wizard
        $('#smartwizard').smartWizard("reset");
        return true;
    });

    $("#prev-btn").on("click", function() {
        // Navigate previous
        $('#smartwizard').smartWizard("prev");
        return true;
    });

    $("#next-btn").on("click", function() {
        // Navigate next
        $('#smartwizard').smartWizard("next");
        return true;
    });
    });
    function guardarResultado(player, paso, grupo) {
        console.log ("+"+player+" "+paso+" "+grupo);
        var ruta = $("#inicio").attr("href");
        var params = {
            'practicas_id': grupo,
            'posicion': paso,
            'user_id': player,
        };
        $.ajax({
            type: 'post',
            url: ruta+"/practicas/actualizar",
            dataType: 'json',
            data: params,
            success: function(data) {
                console.log("llego aqui");
            },
            error: function(error) {
                console.log(error);
            }
        });
    }

</script> 
@endsection