@extends('layoutTemplate')

@section('titulo', "¡Mendeleiev dice!")

@section('contenido')
<section>
<div id="head">
    <div class="line">
        <h1>¡Juega a Mendeleiev dice!</h1>
    </div>
</div>
<div id="content">
    <div class="line">
        <h3>Es fácil: mendeleiev quiere que le sigas el ritmo con sus elementos. Haz click en el que corresponda después de que hay acabado la secuencia ¿Eres capaz de superar los 16 niveles?</h3>
        <div class = "row">
            <div id="juegoQuimica" class = "col-md-6 col-md-offset-1 col-sm-12"></div>
            
        </div>
    </div>
</div>
</section>
    <script src="{{ asset('js/phaser.min.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        var idUsuario = {{session()->get('usuarioId')}};
        var idJuego = 1;
        var juego = new Phaser.Game(800, 600, Phaser.CANVAS, 'juegoQuimica', { preload: preload, create: create, update: update, render: render });
        var contadorGlobal = 0;
        var simon;
        var N = 1;
        var contadorUsuario = 0;
        var contadorActual = 0;
        var contadorSecuencia = 16;
        var secuenciaLista = [];
        var simonDice = false;
        var tiempoEstablecido;
        var cadaElemento;
        var ganador;
        var perdedor;
        var intro;

        //setInterval(function(){puntacion(contadorGlobal);}, 3000);

        //Hemos escogido que haya únicamente seis elementos debido a que especulamos que valores por encima pueden marear al jugador. El 
        //objetivo es que se familiarice con los nombres de los elementos y luego, mediante la práctica, asociarlos.

        function preload() {
            juego.load.image("imgFondo","{{ asset('images/fondoJuego.jpg') }}"); // requiere de meterse en el CREATE o no funciona
            juego.load.spritesheet('elementos', '{{ asset('images/simonAlcalinos.png') }}', 158, 156); // contiene los seis elementos de 160px de ancho y 160px de alto
            //IMPORTANTE: si el tamaño que le asignamos corta alguno de los componentes se repetirá el primero.
        }

        function create() {
            juego.add.tileSprite(0, 0, 800, 600,"imgFondo");
            simon = juego.add.group(); // Añadimos a la variable simon el conjunto que luego usaremos para controlar todos los 
            var objeto;

            for (var i = 0; i < 3; i++)     {
                objeto = simon.create(130 + 190 * i, 156, 'elementos', i); // Crea los elementos de la primera fila (3) que se desplazan a la izquierda
                //A cada uno le asigna un valor de i en el último término que será el de nombre que recibirán (mediante traducción)
                objeto.inputEnabled = true;
                objeto.input.start(0, true);
                objeto.events.onInputDown.add(select); //eventos de input como el ratón: este hace que se ilumine al hacer click y mantenerlo
                objeto.events.onInputUp.add(release); // este hace que se apague
                objeto.events.onInputOut.add(moveOff); //Cuando sale el imput del foco, deja de brillar.
                simon.getAt(i).alpha = 0;//Al elemento i, alpha encarga de asociarle una transparencia de 0 (con lo que es invisible al ppio.
            }

            for (var i = 0; i < 3; i++) {
                objeto = simon.create(130 + 190 * i, 325, 'elementos', i + 3);
                objeto.inputEnabled = true;
                objeto.input.start(0, true);
                objeto.events.onInputDown.add(select);
                objeto.events.onInputUp.add(release); 
                objeto.events.onInputOut.add(moveOff);
                simon.getAt(i + 3).alpha = 0;
            }

            introInicia(); // aqui hace el flash de la animación inicial
            configInicial(); // aquí crea todos y cada uno de los elementos en la lista que se va a a preguntar aleatorizados.
            setTimeout(function(){secuenciaSimon(); intro = false;}, 6000); // Pasados seis segundos después de empezar que se dispare la secuencia del Simon dice

        }

        function update() {

            if (simonDice) //Si el juego está arrancado se evaluará el update.
            {
                if (juego.time.now - tiempoEstablecido >700-N*40) //Esto permite controlar la velocidad del juego: cada vez que se chequea el tiempo se reduce el intervalo en N
                {   
                    simon.getAt(cadaElemento).alpha = .25;//ilumuna la secueccia de elementos dentro del cadaElemento
                    juego.paused = true; //detiene el juego  
                    setTimeout(function() //Dispara cada vez más rápido la función secuenciSimon.
                               {
                        if ( contadorActual< N)
                        {
                            juego.paused = false;
                            contadorGlobal++;
                            secuenciaSimon(); //crea una nueva secuencia

                        }
                        else
                        {
                            simonDice = false;
                            juego.paused = false;
                        }
                    }, 400 - N * 20);
                }
            }
        }

        function render() {

            if (!intro)
            {
                if (simonDice) // Mientras esté activo la construcción de la ronda dirá "simon dice"
                {
                    juego.debug.text('Mendeliev dice', 315, 96, 'rgb(255,0,0)');
                }
                else
                {
                    juego.debug.text('Te toca  Puntuacion: '+contadorGlobal, 280, 96, 'rgb(0,255,0)');
                }
            }
            else
            {
                juego.debug.text('Listo', 360, 96, 'rgb(0,0,255)');
            }

            if (ganador)
            {
                juego.debug.text('¡Ganaste!', 360, 45, 'rgb(0,0,255)');
            }
            else if (perdedor)
            {
                juego.debug.text('¡Perdiste!', 360, 45, 'rgb(0,0,255)');
            }

        }


        //funciones de apoyo


        //Crea la intro
        function introInicia() {

            intro = true;

            for (var i = 0; i < 6; i++) {
                //Con el método add.tween podemos modificar las propiedades de los elementos y, en este caso, crear una transición linal

                // hace la intro mediante un flaheo todos los elementos (cogidos cada uno con el getAt(1) asociado en el create).
                var flasheo = juego.add.tween(simon.getAt(i)).to( { alpha: 1 }, 500, Phaser.Easing.Linear.None, true, 0, 4, true); 

                // Hace el flasheo más rápido hacia el final.
                var final = juego.add.tween(simon.getAt(i)).to( { alpha: .25 }, 500, Phaser.Easing.Linear.None, true);

                flasheo.chain(final); // función que las encadena las dos anteriores
                flasheo.start();
            }
        }

        //Genera la cantidad de elementos que se van a preguntar y los pushea tantas veces como tamaño 

        function configInicial() {
            for (var i = 0; i < contadorSecuencia; i++) {
                elementoSeleccionado = juego.rnd.integerInRange(0,5); //La cantidad de elementos se generan en función de la sequenceCount
                secuenciaLista.push(elementoSeleccionado);
            }
        }

        function secuenciaSimon () {
            simonDice = true; //activa el actualizar
            cadaElemento = secuenciaLista[contadorActual];//Cantidad de elementos  que se van a iluminar por ronda de simon dice
            simon.getAt(cadaElemento).alpha = 1;
            tiempoEstablecido = juego.time.now; // reinicia el contador del tiempo.
            contadorActual++; // aumenta la cantidad de elementos por ronda.
        }

        function secuenciaJugador(seleccionado) {

            elementoCorrecto = secuenciaLista[contadorUsuario];
            contadorUsuario++;
            elementoSeleccionado = simon.getIndex(seleccionado);
            if (elementoSeleccionado == elementoCorrecto)
            {
                if (contadorUsuario == N)
                {
                    if (N == contadorSecuencia)
                    {
                        ganador = true;
                        setTimeout(function(){reiniciar();}, 3000);
                    }
                    else
                    {
                        contadorUsuario = 0;
                        contadorActual = 0;
                        N++;
                        simonDice = true;
                    }
                }
            }
            else
            {
                perdedor = true;
                setTimeout(function(){reiniciar();}, 3000);
            }

        }

        function reiniciar() {
            //Dentro de esta función podemos meter la información del usuario
            //
            guardarResultado(idJuego, contadorGlobal, idUsuario);
            N = 1;
            contadorUsuario = 0;
            contadorActual = 0;
            contadorGlobal = 0;
            secuenciaLista = [];
            ganador = false;
            perdedor = false;
            introInicia(); 
            configInicial(); 
            setTimeout(function(){secuenciaSimon(); intro = false;}, 6000); 

        }


        //Eventos de actúan sobre la selección del usuario.

        function select(objeto, pointer) {

            if (!simonDice && !intro && !perdedor && !ganador)
            {
                objeto.alpha = 1; // Este es el objeto que le pasas desde el select.
            }

        }

        function release(objeto, pointer) {

            if (!simonDice && !intro && !perdedor && !ganador)
            {
                objeto.alpha = .25;
                secuenciaJugador(objeto);
            }
        }

        function moveOff(objeto, pointer) {

            if (!simonDice && !intro && !perdedor && !ganador)
            {
                objeto.alpha = .25;
            }

        }
        // Función ajax que guarda el resultado a partir del obtenido y registrado.
        function guardarResultado(juego, puntuacion, usuario) {
            var ruta = $("#inicio").attr("href");
            var params = {
                'juego_id': juego,
                'puntuacion': puntuacion,
                'user_id': usuario,
            };
            $.ajax({
                type: 'post',
                url: ruta+"/juegos/score",
                dataType: 'json',
                data: params,
                success: function(data) {
                    console.log("llego aqui");
                },
                error: function(error) {
                    console.log(error);
                }
            });
        }
    </script>

</div>

@endsection