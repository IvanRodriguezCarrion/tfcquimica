<!DOCTYPE html>
<html lang="es">
    <head>
        <title>@yield('titulo','Química en estado puro') | Química al alcance de todos</title>
        <meta charset="UTF-8">
        <link rel="shorcut icon" href="{{ asset('images/element.ico') }}">
        <link href="{{ asset('css/old.css') }}" rel="stylesheet" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
        <style>
            body {background: url("{{ asset('images/fondo.png') }}"),repeat;}
        </style>

         <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">
        <meta name="viewport" content="width=device-width, target-densitydpi=160dpi, initial-scale=1.0, maximum-scale=1, user-scalable=no, minimal-ui">
    </head>
    <body >
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Barra de navegación</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a id="inicio" href="{{route('indice')}}">Inicio</a></li>
                        <li><a href="{{route('practica')}}">Zona de testeo</a></li>
                        <li><a href="{{route('juegoSimon')}}">Juegos</a></li>
                        <li><a href="">Sobre nosotros</a></li>
                        
                        @if(session()->get('usuarioMail'))
                            <li><a href="{{route('perfil')}}">{{session()->get('usuarioMail')}}</a></li>
                            <li><a href="{{route('salir')}}">Salir</a></li>
                        @else                  
                            <li><a href="{{route('login')}}" class="pull-right">Acceder</a></li>
                            <li><a href="{{route('registrar')}}">Registrarse</a></li>
                        @endif
                        
                    </ul>
                </div>
            </div>
        </nav>

        @yield('contenido')

        <footer class="col-md-12">
            <hr/><pre> Ringo S.L - Ivan Rodriguez Carrion - Proyecto final de curso - Desarrollo de aplicaciones WEB.</pre>
        </footer>
    </body>
</html>