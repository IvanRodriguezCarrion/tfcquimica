<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[
    'as' => 'indice',
    'uses' => 'ControladorPrincipal@index'
]);

Route::get('practicas',[
    'as' => 'practica',
    'uses' => 'ControladorPrincipal@practicas'
])->middleware("log");


Route::post("user/registrar", [
    'as' => 'user.store',
    'uses' => 'ControladorUser@store'
]);

Route::get('/login',[
    'as' => 'login',
    'uses' => 'ControladorPrincipal@login'
]);

Route::get('practicas/grupo',[
    'as' => 'prueba',
    'uses' => 'ControladorElemento@recuperarTodo'
])->middleware("log");

Route::get('/perfil',[
    'as' => 'perfil',
    'uses' => 'ControladorPrincipal@perfil'
])->middleware("log");

Route::get('/salir',[
    'as' => 'salir',
    'uses' => 'ControladorPrincipal@salir'
])->middleware("log");

Route::get('/registrar',[
    'as' => 'registrar',
    'uses' => 'ControladorPrincipal@registrar'
]);

Route::get('/juegoSimon',[
    'as' => 'juegoSimon',
    'uses' => 'ControladorPrincipal@juegoSimon'
])->middleware("log");

Route::get('/juego2048',[
    'as' => 'juego2048',
    'uses' => 'ControladorPrincipal@juego2048'
])->middleware("log");

Route::post("perfil/actualizar/{id}", [
    'as' => 'user.update',
    'uses' => 'ControladorUser@update'
])->middleware("log");

Route::post("juegos/score", [
    'as' => 'actualizarScore',
    'uses' => 'ControladorEstadisticaJuego@update'
])->middleware("log");

Route::get("estadisticas/alumno", [
    'as' => 'alumnoGraficaPie',
    'uses' => 'ControladorEstadisticaJuego@recuperarGraficaPie'
])->middleware("log");

Route::get("practicas/posicion", [
    'as' => 'recuperarPractica',
    'uses' => 'ControladorEstadisticaPractica@recuperarPosicionPractica'
])->middleware("log");

Route::post("practicas/actualizar", [
    'as' => 'actualizarPractica',
    'uses' => 'ControladorEstadisticaPractica@update'
])->middleware("log");

Route::get("documentacion", [
    'as' => 'documentacion',
    'uses' => 'ControladorPrincipal@documentacion'
]);

Auth::routes();

Route::get('/home', 'HomeController@index');
